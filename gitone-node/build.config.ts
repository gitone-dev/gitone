import { defineBuildConfig } from "unbuild";

export default defineBuildConfig({
  entries: [
    "./src/index",
    {
      builder: "mkdist",
      input: "./src/proto/",
      outDir: "./dist/proto",
    },
  ],
  outDir: "dist",
});
