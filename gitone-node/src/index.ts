import * as grpc from "@grpc/grpc-js";
import pkg from "@vscode/vscode-languagedetection";
import dotdev from "dotenv";
import { HealthImplementation, ServingStatusMap } from "grpc-health-check";
import { bundledLanguages, getHighlighter } from "shiki";
import Highlight from "./highlight";
import {
  HighlightImplementation,
  MarkupImplementation,
  PingImplementation,
} from "./services";

dotdev.config();
const HOST = process.env["HOST"] || "0.0.0.0";
const PORT = Number(process.env["PORT"]) || 50051;
const address = `${HOST}:${PORT}`;

async function main() {
  const highlighter = await getHighlighter({
    themes: ["light-plus"],
    langs: Object.keys(bundledLanguages),
  });

  const modulOperations = new pkg.ModelOperations();
  const highlight = new Highlight(modulOperations, highlighter);

  const server = new grpc.Server();
  const pingImpl = new PingImplementation();
  const highlightImpl = new HighlightImplementation(highlight);
  const markupImpl = new MarkupImplementation(highlight);

  const statusMap: ServingStatusMap = {
    [pingImpl.name()]: "NOT_SERVING",
    [markupImpl.name()]: "NOT_SERVING",
  };
  const healthImpl = new HealthImplementation(statusMap);

  pingImpl.addToServer(server);
  markupImpl.addToServer(server);
  highlightImpl.addToServer(server);
  healthImpl.addToServer(server);

  server.bindAsync(
    address,
    grpc.ServerCredentials.createInsecure(),
    (error, port) => {
      if (error) console.error(error);
      console.log("server is running on", port);
    },
  );
}

main();
