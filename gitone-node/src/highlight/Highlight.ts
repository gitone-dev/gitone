import { ModelOperations } from "@vscode/vscode-languagedetection";
import {
  BundledLanguage,
  Highlighter,
  ThemedToken,
  bundledLanguages,
} from "shiki";
import { reduceThemeColors } from "../split-diffs/ThemeColor";
import { escapeHtml } from "../util/util";

interface HtmlLine {
  number: number;
  text: string;
  html: string;
}

export default class Highlight {
  private _modulOperations: ModelOperations;
  private _highlighter: Highlighter;

  constructor(modulOperations: ModelOperations, highlighter: Highlighter) {
    this._highlighter = highlighter;
    this._modulOperations = modulOperations;
  }

  async language(text: string): Promise<string> {
    const result = await this._modulOperations.runModel(text);
    return result[0]?.languageId || "text";
  }

  async tokensAsync(text: string): Promise<ThemedToken[][] | null> {
    const lang = await this.language(text);
    return this.tokens(lang, text);
  }

  tokens(lang: string, text: string): ThemedToken[][] | null {
    if (!bundledLanguages[lang]) lang = "text";

    try {
      return this._highlighter.codeToTokensBase(text, {
        lang: lang as BundledLanguage,
      });
    } catch (e) {
      console.error(e);
      return null;
    }
  }

  html(lang: string, text: string): string {
    const htmlLines: string[] = [];

    const clrf = text?.includes("\r\n") ? "\r\n" : "\n";
    const tokens = this.tokens(lang, text);
    if (tokens == null) return escapeHtml(text);

    for (let i = 0; i < tokens.length; i++) {
      let html: string = "";
      for (const { content, color, fontStyle } of tokens[i]) {
        const formattedContent = escapeHtml(content);
        if (color || fontStyle) {
          const klass = reduceThemeColors([{ color, fontStyle }]);
          html += `<span class="${klass}">${formattedContent}</span>`;
        } else {
          html += `<span>${formattedContent}</span>`;
        }
      }
      htmlLines.push(html);
    }
    return htmlLines.join(clrf);
  }

  async htmlLines(text: string): Promise<HtmlLine[]> {
    const htmlLines: HtmlLine[] = [];

    const clrf = text?.includes("\r\n") ? "\r\n" : "\n";
    const tokens = await this.tokensAsync(text);
    if (tokens == null) {
      return text.split(clrf).map((html, index) => ({
        number: index + 1,
        text: html + clrf,
        html: escapeHtml(html) + clrf,
      }));
    }

    for (let i = 0; i < tokens.length; i++) {
      let html: string = "";
      let text: string = "";
      for (const { content, color, fontStyle } of tokens[i]) {
        const formattedContent = escapeHtml(content);
        if (color || fontStyle) {
          const klass = reduceThemeColors([{ color, fontStyle }]);
          html += `<span class="${klass}">${formattedContent}</span>`;
        } else {
          html += `<span>${formattedContent}</span>`;
        }
        text += content;
      }
      htmlLines.push({ number: i + 1, text: text + clrf, html: html + clrf });
    }

    return htmlLines;
  }
}
