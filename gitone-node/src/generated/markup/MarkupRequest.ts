// Original file: src/proto/markup.proto


export interface MarkupRequest {
  'fullPath'?: (string);
  'revision'?: (string);
  'path'?: (string);
  'text'?: (string);
}

export interface MarkupRequest__Output {
  'fullPath': (string);
  'revision': (string);
  'path': (string);
  'text': (string);
}
