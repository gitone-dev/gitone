import * as grpc from "@grpc/grpc-js";
import { PackageDefinition, ServiceDefinition } from "@grpc/proto-loader";
import { BlobRequest } from "../generated/highlight/BlobRequest";
import { BlobResponse } from "../generated/highlight/BlobResponse";
import { DiffLine } from "../generated/highlight/DiffLine";
import { DiffRequest } from "../generated/highlight/DiffRequest";
import { DiffResponse } from "../generated/highlight/DiffResponse";
import Highlight from "../highlight";
import { DiffFile } from "../split-diffs/DiffFile";
import { loadProto } from "./loadedProto";

export class HighlightImplementation {
  private _highlight: Highlight;
  private _packageDefinition: PackageDefinition;

  constructor(highlight: Highlight) {
    this._highlight = highlight;
    this._packageDefinition = loadProto("highlight.proto");
  }

  name(): string {
    return "highlight";
  }

  addToServer(server: grpc.Server) {
    const service = this._packageDefinition[
      "highlight.Highlight"
    ] as ServiceDefinition;

    server.addService(service, {
      blob: async (
        call: grpc.ServerUnaryCall<BlobRequest, BlobResponse>,
        callback: grpc.sendUnaryData<BlobResponse>,
      ) => {
        const { text } = call.request;
        const result = await this._highlight.htmlLines(text || "");
        callback(null, { blobLines: result });
      },
      blobs: (call: grpc.ServerDuplexStream<BlobRequest, BlobResponse>) => {
        let index = 1;

        call.on("data", async (data: BlobRequest) => {
          const { text } = data;
          const result = await this._highlight.htmlLines(text || "");
          call.write({ blobLines: result });
          index -= 1;

          if (index === 0) call.end();
        });

        call.on("end", async () => {
          index -= 1;
          if (index === 0) call.end();
        });
      },
      diff: async (
        call: grpc.ServerUnaryCall<DiffRequest, DiffResponse>,
        callback: grpc.sendUnaryData<DiffResponse>,
      ) => {
        const { oldPath, oldText, newPath, newText, lines } = call.request;

        const diffFile = new DiffFile(
          this._highlight,
          oldPath,
          oldText,
          newPath,
          newText,
          { colorWords: true, ignoreCase: false, ignoreWhitespace: false },
          lines || [],
        );

        const diffLines = await diffFile.highlight();
        const result: DiffLine[] = [];
        for (const diffLine of diffLines) {
          result.push(diffLine.to());
        }

        callback(null, { lines: result });
      },
    });
  }
}
