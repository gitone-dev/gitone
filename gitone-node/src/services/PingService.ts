import * as grpc from "@grpc/grpc-js";
import { PackageDefinition, ServiceDefinition } from "@grpc/proto-loader";
import { PingRequest } from "../generated/ping/PingRequest";
import { PingResponse } from "../generated/ping/PingResponse";
import { loadProto } from "./loadedProto";

export class PingImplementation {
  private _packageDefinition: PackageDefinition;

  constructor() {
    this._packageDefinition = loadProto("ping.proto");
  }

  name() {
    return "ping";
  }

  addToServer(server: grpc.Server) {
    const service = this._packageDefinition["ping.Ping"] as ServiceDefinition;

    server.addService(service, {
      Ping: (
        call: grpc.ServerUnaryCall<PingRequest, PingResponse>,
        callback: grpc.sendUnaryData<PingResponse>,
      ) => {
        callback(null, { message: call.request.name });
      },
    });
  }
}
