import * as grpc from "@grpc/grpc-js";
import { PackageDefinition, ServiceDefinition } from "@grpc/proto-loader";
import { MarkupRequest } from "../generated/markup/MarkupRequest";
import { MarkupResponse } from "../generated/markup/MarkupResponse";
import Highlight from "../highlight";
import { HtmlPipeline, Input, Output } from "../htmlPipeline";
import { loadProto } from "./loadedProto";

export class MarkupImplementation {
  private _htmlPipeline: HtmlPipeline;
  private _packageDefinition: PackageDefinition;

  constructor(highlight: Highlight) {
    this._htmlPipeline = new HtmlPipeline(highlight);
    this._packageDefinition = loadProto("markup.proto");
  }

  name() {
    return "markup";
  }

  addToServer(server: grpc.Server) {
    const service = this._packageDefinition[
      "markup.Markup"
    ] as ServiceDefinition;

    server.addService(service, {
      markup: async (
        call: grpc.ServerUnaryCall<MarkupRequest, MarkupResponse>,
        callback: grpc.sendUnaryData<MarkupResponse>,
      ) => {
        const { fullPath, revision, path, text } = call.request;

        const input: Input = { fullPath, revision, path, text };
        const output: Output = new Output();
        this._htmlPipeline.call(input, output);
        const html = output.html || "";

        callback(null, { html });
      },
    });
  }
}
