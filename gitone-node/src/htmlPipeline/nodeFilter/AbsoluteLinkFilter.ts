import { Element } from "domhandler";
import { DomUtils } from "htmlparser2";
import url from "node:url";
import { Input, Output } from "../Filter";
import NodeFilter from "./NodeFilter";

export class AbsoluteLinkFilter extends NodeFilter {
  constructor() {
    super();
  }

  call(input: Input, output: Output) {
    super.call(input, output);

    const { fullPath, revision, path } = input;
    const { document } = output;
    if (!document) return;

    DomUtils.getElementsByTagName("a", document).forEach(function (
      value: Element,
    ) {
      const href = value.attribs["href"];
      if (
        !href ||
        href.startsWith("https://") ||
        href.startsWith("http://") ||
        href.startsWith("//") ||
        href.startsWith("#")
      ) {
        return;
      }

      let absHref = `/${fullPath}/-/blob/${revision}/`;
      if (href.startsWith("/")) {
        absHref = url.resolve(absHref, href.substring(1));
      } else {
        absHref = url.resolve(`${absHref}${path}`, href);
      }
      value.attribs["href"] = absHref;
    });
  }
}
