import { Element } from "domhandler";
import { DomUtils } from "htmlparser2";
import url from "node:url";
import { Input, Output } from "../Filter";
import NodeFilter from "./NodeFilter";

export default class ImageLinkFilter extends NodeFilter {
  constructor() {
    super();
  }

  call(input: Input, output: Output) {
    super.call(input, output);

    const { fullPath, revision, path } = input;
    const { document } = output;
    if (!document) return;

    DomUtils.getElementsByTagName("img", document).forEach(function (
      value: Element,
    ) {
      const src = value.attribs["src"];
      if (
        !src ||
        src.startsWith("https://") ||
        src.startsWith("http://") ||
        src.startsWith("//")
      ) {
        return;
      }

      let absSrc = `/${fullPath}/-/raw/${revision}/`;
      if (src.startsWith("/")) {
        absSrc = url.resolve(absSrc, `.${src}`);
      } else {
        absSrc = url.resolve(`${absSrc}${path}`, src);
      }
      value.attribs["src"] = absSrc;
    });
  }
}
