import { parseDocument } from "htmlparser2";
import Filter, { Input, Output } from "../Filter";

export default class NodeFilter extends Filter {
  constructor() {
    super();
  }

  call(input: Input, output: Output) {
    super.call(input, output);

    if (!output.html) return;
    if (output.document) return;

    output.document = parseDocument(output.html);
  }
}
