import { Element } from "domhandler";
import { DomUtils } from "htmlparser2";
import { Input, Output } from "../Filter";
import NodeFilter from "./NodeFilter";

const TAG_NAMES = ["h1", "h2", "h3", "h4", "h5", "h6"];

function match(tagName: string) {
  return TAG_NAMES.includes(tagName);
}

export default class TableOfContentsFilter extends NodeFilter {
  constructor() {
    super();
  }

  call(input: Input, output: Output) {
    // TODO 优化跳过规则
    if (!!input.path?.endsWith(".adoc")) return;

    super.call(input, output);

    const { document } = output;
    if (!document) return;

    // TODO 优化 ID 生成规则
    const headers: { [key: string]: number } = {};

    DomUtils.getElementsByTagName(match, document).forEach(function (
      value: Element,
    ) {
      value.attribs["class"] = "anchor-header";

      let id = value.attribs["id"];
      if (!id) {
        const title = DomUtils.innerText(value);
        id = `user-content-${encodeURIComponent(title)}`;
      } else if (!id.startsWith("user-content-")) {
        id = `user-content-${id}`
      }

      const index = headers[id];
      if (index) {
        headers[id] = index + 1;
        id = `${id}-${index}`;
      } else {
        headers[id] = 2;
      }

      value.attribs["id"] = id;

      DomUtils.prependChild(
        value,
        new Element("a", { href: `#${id}`, class: "anchor" }),
      );
    });
  }
}
