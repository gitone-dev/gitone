import render from "dom-serializer";
import Highlight from "../highlight";
import Filter, { Input, Output } from "./Filter";
import AsciidocFilter from "./convertFilter/AsciidocFilter";
import MarkdownFilter from "./convertFilter/MarkdownFilter";
import { AbsoluteLinkFilter } from "./nodeFilter/AbsoluteLinkFilter";
import ImageLinkFilter from "./nodeFilter/ImageLinkFilter";
import TableOfContentsFilter from "./nodeFilter/TableOfContentsFilter";

export default class HtmlPipeline extends Filter {
  private _filters: Filter[];

  constructor(highlight: Highlight) {
    super();
    this._filters = [
      new MarkdownFilter(highlight),
      new AsciidocFilter(highlight),
      new AbsoluteLinkFilter(),
      new ImageLinkFilter(),
      new TableOfContentsFilter(),
    ];
  }

  call(input: Input, output: Output) {
    this._filters.forEach((filter) => filter.call(input, output));

    if (!output.document) return;
    output.html = render(output.document);
  }
}
