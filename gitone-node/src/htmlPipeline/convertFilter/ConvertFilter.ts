import Filter, { Input, Output } from "../Filter";

export default class ConvertFilter extends Filter {
  constructor() {
    super();
  }

  call(input: Input, output: Output) {
    super.call(input, output);
  }

  match(input: Input) {
    return false;
  }
}
