import MarkdownIt from "markdown-it/lib/index.mjs";
import Highlight from "../../highlight";
import { Input, Output } from "../Filter";
import ConvertFilter from "./ConvertFilter";

export default class MarkdownFilter extends ConvertFilter {
  private _markdownIt: MarkdownIt;

  constructor(highlight: Highlight) {
    super();
    this._markdownIt = MarkdownIt({
      highlight: (str, lang) => {
        return `<pre class="shiki highlight"><code data-lang="${encodeURIComponent(lang)}">` +
          `${highlight.html(lang.toLowerCase(), str)}` +
          `</code></pre>`
      },
    });
  }

  match(input: Input): boolean {
    return !!input.path?.endsWith(".md");
  }

  call(input: Input, output: Output) {
    super.call(input, output);
    if (!this.match(input)) return;

    if (!input.text) return;
    if (output.html) return;

    output.html = this._markdownIt.render(input.text);
  }
}
