import Processor, { Asciidoctor, Block, SyntaxHighlighter } from "asciidoctor";
import Highlight from "../../highlight";
import { Input, Output } from "../Filter";
import ConvertFilter from "./ConvertFilter";

export default class AsciidocFilter extends ConvertFilter {
  private _asciidoctor: Asciidoctor;

  constructor(highlight: Highlight) {
    super();
    this._asciidoctor = Processor();
    this._asciidoctor.SyntaxHighlighter.register("shiki", {
      initialize(name: string, backend: string, opts: any) {
        this.super(name, backend, opts);
      },
      highlight(
        this: SyntaxHighlighter,
        _node: Block,
        source: string,
        lang: string,
      ) {
        return highlight.html(lang, source);
      },
      handlesHighlighting() {
        return true;
      },
    });
  }

  match(input: Input): boolean {
    return !!input.path?.endsWith(".adoc");
  }

  call(input: Input, output: Output) {
    super.call(input, output);
    if (!this.match(input)) return;

    if (!input.text) return;
    if (output.html) return;

    output.html = this._asciidoctor
      .convert(input.text, {
        safe: "secure",
        doctype: "article",
        attributes: {
          showTitle: true,
          icons: "font",
          idPrefix: "user-content-",
          idSeparator: "-",
          sectAnchors: "$",
          env: "github",
          "env-github": "",
          "source-highlighter": "shiki",
        },
      })
      .toString();
  }
}
