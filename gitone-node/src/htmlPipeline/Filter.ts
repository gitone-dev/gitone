import { Document } from "domhandler";

export class Input {
  fullPath?: string;
  revision?: string;
  path?: string;
  text?: string;
}

export class Output {
  html?: string;
  document?: Document;
}

export default class Filter {
  constructor() {
  }

  call(input: Input, output: Output) { }
}
