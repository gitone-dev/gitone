import react from "@vitejs/plugin-react";
import history from "connect-history-api-fallback";
import { Request, Response } from "express-serve-static-core";
import path from "path";
import { ViteDevServer, defineConfig } from "vite";

function rewriteAll() {
  return {
    name: "rewrite-all",
    configureServer(server: ViteDevServer) {
      return () => {
        const handler = history({
          disableDotRule: true,
          rewrites: [{ from: /\/$/, to: () => "/index.html" }]
        });

        server.middlewares.use((req, res, next) => {
          handler(req as Request, res as Response, next)
        });
      };
    }
  }
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), rewriteAll()],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
    },
  },
  server: {
    proxy: {
      "/.well-known/": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "/oauth2/": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "/connect/": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "/userinfo": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "/login": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "/logout": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "/graphql": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "/avatars": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "/rest/": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "^(/[0-9a-zA-Z]+){2,5}/-/archive|raw/.*": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
      "^(/[0-9a-zA-Z]+){2,5}/-/releases/download/.*": {
        target: "http://127.0.0.1:8080",
        changeOrigin: false,
        secure: false,
      },
    },
  },
});
