import { useTreeQuery } from "@/generated/types";
import ChunkPaper from "@/shared/ChunkPaper";
import ErrorBox from "@/shared/ErrorBox";
import LoadingBox from "@/shared/LoadingBox";
import Paper from "@mui/material/Paper";
import { useEffect } from "react";
import CommitPaper from "../blob/CommitPaper";
import Markup from "../blob/Markup";
import TableTree from "./TableTree";

interface Props {
  fullPath: string;
  revision: string;
  path: string;
}

export default function TreeContainer(props: Props) {
  const { fullPath, revision, path } = props;

  const { data, loading, error, fetchMore } = useTreeQuery({
    fetchPolicy: "network-only",
    variables: { fullPath, revision, path },
  });

  const commit = data?.repository.commits?.edges?.at(0)?.node;
  const edges = data?.repository?.tree?.edges;
  const pageInfo = data?.repository?.tree?.pageInfo;
  const policy = data?.namespacePolicy;
  const readme = data?.repository.readme;

  const onScroll = () => {
    if (loading) return;
    if (!pageInfo?.hasNextPage) return;

    const { scrollTop, scrollHeight, clientHeight } = document.documentElement;
    if (scrollTop + clientHeight + 16 < scrollHeight) return;

    fetchMore({ variables: { after: pageInfo?.endCursor } });
  };

  useEffect(() => {
    onScroll();
    window.addEventListener("scroll", onScroll);
    return () => {
      window.removeEventListener("scroll", onScroll);
    };
  });

  if (loading) {
    return <LoadingBox />;
  } else if (error) {
    return <ErrorBox message={error.message} />;
  } else if (!edges || !pageInfo || !policy) {
    return <ErrorBox message="客户端查询条件错误" />;
  }

  return (
    <>
      {commit && (
        <CommitPaper
          fullPath={fullPath}
          revision={revision}
          path={path}
          commit={commit}
        />
      )}
      <Paper variant="outlined">
        <TableTree
          fullPath={fullPath}
          revision={revision}
          edges={edges}
          pageInfo={pageInfo}
          loadMore={onScroll}
        />
      </Paper>
      {readme?.rich && (
        <ChunkPaper primary="README">
          <Markup contentType={readme.contentType} rich={readme?.rich} />
        </ChunkPaper>
      )}
    </>
  );
}
