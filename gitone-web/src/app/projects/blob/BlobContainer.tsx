import { useBlobQuery } from "@/generated/types";
import ErrorBox from "@/shared/ErrorBox";
import LoadingBox from "@/shared/LoadingBox";
import Box from "@mui/material/Box";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import CommitPaper from "./CommitPaper";
import Content from "./Content";
import Header, { ViewMode } from "./Header";

interface Props {
  fullPath: string;
  revision: string;
  path: string;
}

export default function BlobContainer(props: Props) {
  const { fullPath, revision, path } = props;

  const [value, setValue] = useState<ViewMode>(null);

  const navigate = useNavigate();
  const { data, loading, error } = useBlobQuery({
    variables: { fullPath, revision, path },
    onCompleted(data) {
      if (value) return;
      const blob = data.repository.blob;
      if (blob?.rich || blob?.contentType?.startsWith("image/")) {
        setValue("priview");
      } else {
        setValue("code");
      }
    },
  });

  const commit = data?.repository.commits?.edges?.at(0)?.node;
  const blob = data?.repository?.blob;
  const policy = data?.namespacePolicy;

  const onChange = (value: ViewMode) => {
    if (!value) return;

    setValue(value);
    if (value === "blame") {
      const blameHref = `/${fullPath}/-/blame/${revision}/${path}`;
      navigate(blameHref);
    }
  };

  if (loading) {
    return <LoadingBox />;
  } else if (error) {
    return <ErrorBox message={error.message} />;
  } else if (!blob || !policy) {
    return <ErrorBox message="查询错误" />;
  }

  return (
    <Box mt={2}>
      {commit && (
        <CommitPaper
          fullPath={fullPath}
          revision={revision}
          path={path}
          commit={commit}
        />
      )}
      <Header
        fullPath={fullPath}
        revision={revision}
        path={path}
        blob={blob}
        viewMode={value}
        onChange={onChange}
      />
      <Content
        fullPath={fullPath}
        revision={revision}
        path={path}
        blob={blob}
        viewMode={value}
      />
    </Box>
  );
}
