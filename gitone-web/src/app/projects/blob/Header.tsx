import { Blob } from "@/generated/types";
import { isImage, isText } from "@/utils/git";
import { humanReadable } from "@/utils/size";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import Typography from "@mui/material/Typography";
import { MouseEvent } from "react";

interface Props {
  fullPath: string;
  revision: string;
  path: string;
  blob: Blob;
  viewMode: ViewMode;
  onChange: (value: ViewMode) => void;
}

export type ViewMode = "priview" | "code" | "blame" | null;

export default function Header(props: Props) {
  const { fullPath, revision, path, blob, viewMode, onChange } = props;

  const rawHref = `/${fullPath}/-/raw/${revision}/${path}`;
  const viewPriview = isImage(blob) || !!blob.rich;
  const viewCode = isText(blob);

  const handleChange = (_event: MouseEvent<HTMLElement>, value: ViewMode) => {
    onChange(value);
  };

  return (
    <Paper
      variant="outlined"
      sx={{
        px: 2,
        py: 1,
        borderRadius: "5px 5px 0 0",
        backgroundColor: "rgb(246, 248, 250)",
      }}
    >
      <Stack
        spacing={1}
        direction="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Stack direction="row" spacing={1}>
          <ToggleButtonGroup
            exclusive
            size="small"
            onChange={handleChange}
            value={viewMode}
          >
            {viewPriview && (
              <ToggleButton sx={{ py: 0 }} value="priview">
                预览
              </ToggleButton>
            )}
            {viewCode && (
              <ToggleButton sx={{ py: 0 }} value="code">
                源码
              </ToggleButton>
            )}
            {viewCode && (
              <ToggleButton sx={{ py: 0 }} value="blame">
                Blame
              </ToggleButton>
            )}
          </ToggleButtonGroup>
          {blob.viewable && (
            <Typography color="text.secondary" lineHeight={2}>
              {blob.linesCount} lines
            </Typography>
          )}
          <Typography color="text.secondary" lineHeight={2}>
            {humanReadable(blob.size)}
          </Typography>
        </Stack>
        <Button href={rawHref} size="small" variant="outlined">
          Raw
        </Button>
      </Stack>
    </Paper>
  );
}
