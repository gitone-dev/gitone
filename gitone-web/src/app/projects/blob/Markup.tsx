import "font-awesome/css/font-awesome.css";
import "./asciidoctor.css";
import "./markup.css";

interface Props {
  contentType: string | null | undefined;
  rich: string;
}

export default function Markup(props: Props) {
  const { rich, contentType } = props;

  const className = contentType === "text/x-asciidoc" ? "asciidoc" : "markup";

  return (
    <article dangerouslySetInnerHTML={{ __html: rich }} className={className} />
  );
}
