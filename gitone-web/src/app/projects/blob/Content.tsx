import { Blob } from "@/generated/types";
import { isBinary, isImage, isText } from "@/utils/git";
import { humanReadable } from "@/utils/size";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import { ViewMode } from "./Header";
import Markup from "./Markup";
import Text from "./Text";

interface Props {
  fullPath: string;
  revision: string;
  path: string;
  blob: Blob;
  viewMode: ViewMode;
}

export default function Content(props: Props) {
  const { fullPath, revision, path, blob, viewMode } = props;

  const edges = blob.lines?.edges;
  const rich = blob.rich;

  const rawHref = `/${fullPath}/-/raw/${revision}/${path}`;
  const viewRich = viewMode === "priview" && isText(blob) && !!rich;
  const viewCode = viewMode === "code" && isText(blob) && edges;
  const viewImage = viewMode === "priview" && isImage(blob);
  const viewBinary = isBinary(blob);

  return (
    <Paper
      variant="outlined"
      sx={{ px: 2, pb: 2, pt: 1, borderRadius: 0, overflow: "auto" }}
    >
      {viewRich && <Markup contentType={blob.contentType} rich={rich} />}
      {viewCode && <Text edges={edges} />}
      <Box textAlign="center">
        {viewImage && (
          <Box component="img" sx={{ maxWidth: "100%" }} src={rawHref} />
        )}
        {viewBinary && (
          <Link href={rawHref} color="text.primary" underline="hover">
            原始文件（{humanReadable(blob.size)}）
          </Link>
        )}
      </Box>
    </Paper>
  );
}
