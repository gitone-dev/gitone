import Box from "@mui/material/Box";

interface Props {
  html: string;
  borderTop?: string;
}

export default function TdHtml(props: Props) {
  const { html, borderTop } = props;

  return (
    <Box
      component="td"
      className="code"
      sx={{
        py: 0,
        fontFamily: "monospace",
        whiteSpace: "pre",
        borderTop: borderTop,
      }}
      dangerouslySetInnerHTML={{ __html: html }}
    />
  );
}
