import { Commit } from "@/generated/types";
import CommitTip from "@/shared/CommitTip";
import RelativeTime from "@/shared/RelativeTime";
import { SHA_ABBR_LENGTH } from "@/utils/git";
import HistoryIcon from "@mui/icons-material/History";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { Link as RouterLink } from "react-router-dom";

interface Props {
  fullPath: string;
  path: string;
  revision: string;
  commit: Commit;
}

export default function CommitPaper(props: Props) {
  const { fullPath, path, revision, commit } = props;

  return (
    <Paper variant="outlined" sx={{ my: 2, px: 2, py: 1 }}>
      <Stack
        direction="row"
        spacing={1}
        alignItems="center"
        justifyContent="space-between"
      >
        <Stack direction="row" spacing={1} flexGrow={1} width={0}>
          <Typography
            color="text.secondary"
            variant="body2"
            whiteSpace="nowrap"
          >
            {commit?.committer?.name} 提交于
          </Typography>
          <Typography color="text.secondary" variant="body2">
            <RelativeTime date={commit?.committer?.date} />
          </Typography>
          <CommitTip
            fullPath={fullPath}
            commit={commit}
            color="text.secondary"
            variant="body2"
          />
        </Stack>
        <Stack direction="row" spacing={1} alignItems="center">
          <Link
            color="text.secondary"
            component={RouterLink}
            to={`/${fullPath}/-/commit/${commit?.sha}`}
            underline="hover"
            variant="body2"
          >
            {commit.sha.substring(0, SHA_ABBR_LENGTH)}
          </Link>
          <Button
            component={RouterLink}
            to={`/${fullPath}/-/commits/${revision}/${path}`}
            size="small"
            startIcon={<HistoryIcon fontSize="small" />}
          >
            历史
          </Button>
        </Stack>
      </Stack>
    </Paper>
  );
}
