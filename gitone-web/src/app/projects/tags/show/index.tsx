import ErrorPage from "@/app/ErrorPage";
import { Action, useDeleteTagMutation, useTagQuery } from "@/generated/types";
import ErrorBox from "@/shared/ErrorBox";
import LoadingBox from "@/shared/LoadingBox";
import { useFullPath } from "@/utils/router";
import { useSnackbar } from "notistack";
import { useNavigate } from "react-router-dom";
import TagPaper from "./TagPaper";

export default function Show() {
  const { fullPath, star: name } = useFullPath();
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const { data, loading, error } = useTagQuery({
    variables: { fullPath, name },
  });

  const [deleteTagMutation] = useDeleteTagMutation({
    variables: {
      input: { fullPath, name },
    },
    onCompleted() {
      enqueueSnackbar("删除成功", { variant: "success" });
      navigate(`/${fullPath}/-/tags`);
    },
    onError(error) {
      enqueueSnackbar(error.message, { variant: "error" });
    },
  });

  const tag = data?.repository.tag;
  const policy = data?.namespacePolicy;

  if (loading) {
    return <LoadingBox />;
  } else if (error) {
    return <ErrorBox message={error.message} />;
  } else if (!tag) {
    return <ErrorPage message="查询出错" />;
  } else if (!policy?.actions?.includes(Action.Read)) {
    return <ErrorPage message="无权限" />;
  }

  const onDelete = () => deleteTagMutation();

  return (
    <TagPaper
      fullPath={fullPath}
      policy={policy}
      tag={tag}
      onDelete={onDelete}
    />
  );
}
