import { Action, Policy, Tag } from "@/generated/types";
import ChunkPaper from "@/shared/ChunkPaper";
import ListAsset from "@/shared/ListAsset";
import RelativeTime from "@/shared/RelativeTime";
import { SHA_ABBR_LENGTH } from "@/utils/git";
import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";
import Divider from "@mui/material/Divider";
import Link from "@mui/material/Link";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { Link as RouterLink } from "react-router-dom";

interface Props {
  fullPath: string;
  policy: Policy;
  tag: Tag;
  onDelete: () => void;
}

export default function TagPaper(props: Props) {
  const {
    fullPath,
    policy: { actions },
    tag,
    onDelete,
  } = props;

  return (
    <ChunkPaper
      primary={tag.name || ""}
      action={
        <ButtonGroup size="small">
          {!tag.isRelease && (
            <Button
              disabled={!actions.includes(Action.Update)}
              component={RouterLink}
              to={`/${fullPath}/-/releases/new`}
              state={tag}
            >
              新建发布
            </Button>
          )}
          {!tag.isRelease && (
            <Button
              disabled={!actions.includes(Action.Update)}
              onClick={onDelete}
            >
              删除标签
            </Button>
          )}
          {tag.isRelease && (
            <Button
              component={RouterLink}
              to={`/${fullPath}/-/releases/tag/${tag.name}`}
            >
              查看发布
            </Button>
          )}
        </ButtonGroup>
      }
    >
      <Stack direction="row" spacing={1}>
        {tag.tagger ? (
          <>
            <Typography color="text.secondary" variant="body2">
              {tag.tagger.name} 标记于
            </Typography>
            <Typography color="text.secondary" variant="body2">
              <RelativeTime date={tag.tagger.date} />
            </Typography>
            <Link
              color="text.secondary"
              component={RouterLink}
              to={`/${fullPath}/-/commit/${tag.commit?.sha}`}
              underline="hover"
              variant="body2"
            >
              <code>{tag.commit?.sha.substring(0, SHA_ABBR_LENGTH)}</code>
            </Link>
          </>
        ) : (
          <>
            <Link
              color="text.secondary"
              component={RouterLink}
              to={`/${fullPath}/-/commit/${tag.commit?.sha}`}
              underline="hover"
              variant="body2"
            >
              <code>{tag.commit?.sha.substring(0, SHA_ABBR_LENGTH)}</code>
            </Link>
            <Typography color="text.secondary" variant="body2">
              {tag.commit?.committer?.name} 提交于
            </Typography>
            <Typography color="text.secondary" variant="body2">
              <RelativeTime date={tag.commit?.committer?.date} />
            </Typography>
          </>
        )}
      </Stack>
      <Typography sx={{ whiteSpace: "pre-wrap", mt: 2 }} variant="body2">
        {tag.fullMessage || tag.commit?.fullMessage}
      </Typography>
      <Divider sx={{ my: 2 }} />
      <ListAsset fullPath={fullPath} tag={tag} edges={undefined} />
    </ChunkPaper>
  );
}
