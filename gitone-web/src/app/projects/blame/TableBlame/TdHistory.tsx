import FastRewindOutlinedIcon from "@mui/icons-material/FastRewindOutlined";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { Link as RouterLink } from "react-router-dom";

interface Props {
  fullPath: string;
  revision: string;
  path: string;
  rowSpan: number | undefined;
  borderTop?: string;
  disabled?: boolean;
}

export default function TdHistory(props: Props) {
  const { fullPath, revision, path, rowSpan, borderTop, disabled } = props;

  return (
    <Box
      component="td"
      rowSpan={rowSpan}
      sx={{
        color: "text.secondary",
        fontSize: "0.9em",
        overflow: "hidden",
        width: 64,
        textOverflow: "ellipsis",
        userSelect: "none",
        borderTop: borderTop,
      }}
    >
      <Button
        disabled={disabled}
        size="small"
        sx={{ p: 0 }}
        component={RouterLink}
        to={`/${fullPath}/-/blame/${revision}/${path}`}
      >
        <FastRewindOutlinedIcon fontSize="small" />
      </Button>
    </Box>
  );
}
