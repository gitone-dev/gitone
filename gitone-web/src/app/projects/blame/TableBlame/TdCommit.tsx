import { Commit } from "@/generated/types";
import CommitTip from "@/shared/CommitTip";
import Box from "@mui/material/Box";

interface Props {
  fullPath: string;
  revision: string;
  path: string;
  commit: Commit | null | undefined;
  rowSpan: number | undefined;
  borderTop?: string;
}

export default function TdCommit(props: Props) {
  const { fullPath, commit, rowSpan, borderTop } = props;
  return (
    <Box
      component="td"
      rowSpan={rowSpan}
      sx={{
        color: "text.secondary",
        fontSize: "0.9em",
        overflow: "hidden",
        width: 240,
        textOverflow: "ellipsis",
        userSelect: "none",
        borderTop: borderTop,
      }}
    >
      <CommitTip fullPath={fullPath} commit={commit} />
    </Box>
  );
}
