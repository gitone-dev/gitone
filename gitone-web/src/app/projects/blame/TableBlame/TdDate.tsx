import RelativeTime from "@/shared/RelativeTime";
import Box from "@mui/material/Box";

interface Props {
  date: string;
  rowSpan?: number | undefined;
  borderTop?: string;
}

export default function TdDate(props: Props) {
  const { date, rowSpan, borderTop } = props;

  return (
    <Box
      component="td"
      rowSpan={rowSpan}
      sx={{
        color: "text.secondary",
        fontSize: "0.9em",
        userSelect: "none",
        borderTop: borderTop,
        width: 100,
      }}
    >
      <RelativeTime date={date} />
    </Box>
  );
}
