import { BlameLine, BlobLine } from "@/generated/types";
import Box from "@mui/material/Box";
import TdHtml from "../../blob/TableCode/TdHtml";
import TdNumber from "../../blob/TableCode/TdNumber";
import TdCommit from "./TdCommit";
import TdDate from "./TdDate";
import TdHistory from "./TdHistory";

export type Range = [number, number];

interface Props {
  fullPath: string;
  revision: string;
  path: string;
  blobLine: BlobLine;
  blameLine: BlameLine | null | undefined;
}

export default function TrLine(props: Props) {
  const {
    fullPath,
    revision,
    path,
    blameLine,
    blobLine: { number, html },
  } = props;

  const borderTop = number === 1 || !blameLine ? undefined : "1px solid #ccc";
  const rowSpan = blameLine
    ? (blameLine.right || 0) - (blameLine.left || 0)
    : undefined;

  return (
    <Box component="tr" id={`LC${number}`}>
      {blameLine && (
        <TdDate
          date={blameLine?.commit?.committer?.date}
          rowSpan={rowSpan}
          borderTop={borderTop}
        />
      )}
      {blameLine && (
        <TdCommit
          fullPath={fullPath}
          revision={revision}
          path={path}
          commit={blameLine.commit}
          rowSpan={rowSpan}
          borderTop={borderTop}
        />
      )}
      {blameLine && (
        <TdHistory
          fullPath={fullPath}
          path={blameLine.sourcePath || ""}
          revision={blameLine.commit?.sha || ""}
          rowSpan={rowSpan}
          borderTop={borderTop}
          disabled={blameLine.commit?.sha === revision}
        />
      )}
      <TdNumber number={number || 0} borderTop={borderTop} />
      <TdHtml html={html || ""} borderTop={borderTop} />
    </Box>
  );
}
