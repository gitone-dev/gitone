import { useBlameQuery } from "@/generated/types";
import ErrorBox from "@/shared/ErrorBox";
import LoadingBox from "@/shared/LoadingBox";
import Box from "@mui/material/Box";
import { useNavigate } from "react-router-dom";
import CommitPaper from "../blob/CommitPaper";
import Header, { ViewMode } from "../blob/Header";
import Content from "./Content";

interface Props {
  fullPath: string;
  revision: string;
  path: string;
}

export default function BlameContainer(props: Props) {
  const { fullPath, revision, path } = props;

  const navigate = useNavigate();
  const { data, loading, error } = useBlameQuery({
    variables: { fullPath, revision, path },
  });

  const commit = data?.repository.commits?.edges?.at(0)?.node;
  const blame = data?.repository?.blame;
  const blob = data?.repository?.blob;
  const policy = data?.namespacePolicy;

  const onChange = (value: ViewMode) => {
    if (!value || value === "blame") return;
    const blobHref = `/${fullPath}/-/blob/${revision}/${path}`;
    navigate(blobHref);
  };

  if (loading) {
    return <LoadingBox />;
  } else if (error) {
    return <ErrorBox message={error.message} />;
  } else if (!blame || !blob || !policy) {
    return <ErrorBox message="查询错误" />;
  }

  return (
    <Box mt={2}>
      {commit && (
        <CommitPaper
          fullPath={fullPath}
          revision={revision}
          path={path}
          commit={commit}
        />
      )}
      <Header
        fullPath={fullPath}
        revision={revision}
        path={path}
        blob={blob}
        viewMode="blame"
        onChange={onChange}
      />
      <Content
        fullPath={fullPath}
        revision={revision}
        path={path}
        blame={blame}
        blob={blob}
        viewMode="blame"
      />
    </Box>
  );
}
