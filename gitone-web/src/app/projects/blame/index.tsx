import { useRevisionPathQuery } from "@/generated/types";
import { BreadcrumbItems } from "@/layout/Breadcrumbs";
import ErrorBox from "@/shared/ErrorBox";
import LoadingBox from "@/shared/LoadingBox";
import RevisionPath from "@/shared/RevisionPath";
import { useFullPath } from "@/utils/router";
import BlameContainer from "./BlameContainer";

const breadcrumbItems = (
  fullPath: string,
  revision: string,
  path: string
): BreadcrumbItems => {
  const items = [];
  const paths = path.split("/");

  for (let i = 1; i < paths.length; i++) {
    const path = paths.slice(0, i).join("/");
    items.push({
      to: `/${fullPath}/-/tree/${revision}/${path}`,
      text: paths[i - 1],
    });
  }
  items.push({
    to: `/${fullPath}/-/blame/${revision}/${path}`,
    text: paths[paths.length - 1],
  });

  return {
    [`/${fullPath}/-/blame/${revision}/${path}`]: [...items],
  };
};

export default function Show() {
  const { fullPath, star } = useFullPath();
  const { data, loading, error } = useRevisionPathQuery({
    variables: { fullPath, revisionPath: star },
  });

  const { type, revision, path } = data?.repository.revisionPath || {};

  if (loading) {
    return <LoadingBox />;
  } else if (error) {
    return <ErrorBox message={error.message} />;
  } else if (!revision || path === undefined || type !== "blob") {
    return <ErrorBox message="查询错误" />;
  }

  return (
    <>
      <RevisionPath
        fullPath={fullPath}
        type="blame"
        revision={revision}
        path={path}
        breadcrumbItems={breadcrumbItems(fullPath, revision, path)}
      />
      <BlameContainer fullPath={fullPath} revision={revision} path={path} />
    </>
  );
}
