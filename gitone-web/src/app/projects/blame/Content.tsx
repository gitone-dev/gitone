import { Blame, BlameLine, Blob, BlobLine } from "@/generated/types";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { ViewMode } from "../blob/Header";
import TrLine from "./TableBlame/TrLine";

interface Props {
  fullPath: string;
  revision: string;
  path: string;
  blame: Blame;
  blob: Blob;
  viewMode: ViewMode;
}

export default function Content(props: Props) {
  const { fullPath, revision, path, blame, blob } = props;

  const blobLines = (blameLine: BlameLine): BlobLine[] => {
    const { left, right } = blameLine;

    if (typeof left !== "number" || typeof right !== "number") {
      return [];
    }

    return (blob.lines?.edges || [])
      .slice(left, right)
      .map((edge) => edge.node);
  };

  return (
    <Paper
      variant="outlined"
      sx={{ px: 2, pb: 2, pt: 1, borderRadius: 0, overflow: "auto" }}
    >
      <Box component="table" sx={{ width: "100%", tableLayout: "fixed" }}>
        <tbody>
          {blame.lines?.map((blameLine) =>
            blobLines(blameLine).map((blobLine, idx) => (
              <TrLine
                key={blobLine.number}
                fullPath={fullPath}
                revision={revision}
                path={path}
                blobLine={blobLine}
                blameLine={idx === 0 ? blameLine : undefined}
              />
            ))
          )}
        </tbody>
      </Box>
    </Paper>
  );
}
