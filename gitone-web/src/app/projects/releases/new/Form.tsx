import {
  CreateReleaseInput,
  ReleaseState,
  Tag,
  TagEdge,
  TagFilter,
  UpdateReleaseInput,
  useCreateReleaseMutation,
  useReleaseLazyQuery,
  useTagsLazyQuery,
  useUpdateReleaseMutation
} from "@/generated/types";
import ChunkPaper from "@/shared/ChunkPaper";
import { release as pattern } from "@/utils/regex";
import Autocomplete from "@mui/material/Autocomplete";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import TextField from "@mui/material/TextField";
import debounce from "@mui/material/utils/debounce";
import { Maybe } from "graphql/jsutils/Maybe";
import { useSnackbar } from "notistack";
import { useMemo, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import AssetForm from "./AssetForm";

interface Props {
  fullPath: string;
  tag?: Tag;
}

export default function Form(props: Props) {
  const { fullPath, tag } = props;
  const tagEdge: TagEdge | null = tag ? { cursor: tag.id, node: tag } : null;
  const [options, setOptions] = useState<TagEdge[]>(tagEdge ? [tagEdge] : []);
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const {
    control,
    formState: { errors },
    handleSubmit,
    register,
    setValue,
  } = useForm<CreateReleaseInput>({
    defaultValues: {
      tagName: tag?.name,
    },
  });

  const [releaseLazyQuery, { data }] = useReleaseLazyQuery();
  const [tagsLazyQuery, { loading }] = useTagsLazyQuery({
    fetchPolicy: "network-only",
  });

  const [createReleaseMutation] = useCreateReleaseMutation({
    onCompleted(data) {
      const release = data?.payload?.release;
      if (!release?.id || release.state === ReleaseState.Draft) return;

      enqueueSnackbar("创建成功", { variant: "success" });
      navigate(`/${fullPath}/-/releases/tag/${release.tag?.name}`, {
        replace: true,
      });
    },
    onError(error) {
      enqueueSnackbar(error.message, { variant: "error" });
    },
  });
  const [updateReleaseMutation] = useUpdateReleaseMutation({
    onCompleted(data) {
      const release = data?.payload?.release;
      if (!release?.id || release.state === ReleaseState.Draft) return;

      enqueueSnackbar("创建成功", { variant: "success" });
      navigate(`/${fullPath}/-/releases/tag/${release.tag?.name}`, {
        replace: true,
      });
    },
    onError(error) {
      enqueueSnackbar(error.message, { variant: "error" });
    },
  });

  const tagsQuery = useMemo(
    () =>
      debounce((query: string) => {
        const filterBy: TagFilter = { query };
        tagsLazyQuery({
          variables: { fullPath, first: 20, filterBy },
          onCompleted(data) {
            setOptions(data.repository.tags?.edges || []);
          },
        });
      }, 500),
    [tagsLazyQuery]
  );

  const onChange = (_event: unknown, edge: Maybe<TagEdge>) => {
    if (!edge?.node.name) return;

    setValue("tagName", edge.node.name);
  };

  const onInputChange = (_event: unknown, value: string) => {
    tagsQuery(value);
  };

  const onChangeState = (_event: unknown, checked: boolean) => {
    if (checked) {
      setValue("latest", false);
      setValue("state", ReleaseState.Preview);
    } else {
      setValue("state", ReleaseState.Published);
    }
  };

  const onChangeLatest = (_event: unknown, checked: boolean) => {
    if (checked) {
      setValue("latest", true);
      setValue("state", ReleaseState.Published);
    } else {
      setValue("latest", false);
    }
  };

  const onCreate = handleSubmit((input: CreateReleaseInput) => {
    if (data?.release) {
      const updateInput: UpdateReleaseInput = {
        id: data.release.id,
        tagName: input.tagName,
        title: input.title,
        description: input.description,
        state: input.state,
        latest: input.latest,
      };
      updateReleaseMutation({
        variables: { input: updateInput },
      });
    } else {
      createReleaseMutation({ variables: { input } });
    }
  });

  return (
    <ChunkPaper primary="新发布" component="form" onSubmit={onCreate}>
      <TextField
        sx={{ display: "none" }}
        {...register("fullPath", { value: fullPath })}
      />
      <Autocomplete
        defaultValue={tagEdge}
        fullWidth
        sx={{ mt: 2 }}
        loading={loading}
        getOptionLabel={(option) => option.node.name || ""}
        filterOptions={(x) => x}
        options={options}
        loadingText="加载中"
        autoComplete
        includeInputInList
        noOptionsText="无"
        onChange={onChange}
        isOptionEqualToValue={(option, value) =>
          option.node.name === value.node.name
        }
        onInputChange={onInputChange}
        renderInput={(params) => (
          <TextField {...params} required size="small" label="标签" />
        )}
        renderOption={(props, option) => {
          return (
            <ListItem {...props}>
              <ListItemText primary={option.node.name} />
            </ListItem>
          );
        }}
      />
      <TextField
        error={Boolean(errors.title)}
        fullWidth
        helperText={errors.title?.message || pattern.title.helper}
        label="标题"
        margin="dense"
        required
        size="small"
        {...register("title", { ...pattern.title.rules })}
      />
      <TextField
        error={Boolean(errors.description)}
        fullWidth
        helperText={errors.description?.message || pattern.description.helper}
        label="描述"
        multiline
        rows={5}
        {...register("description", { ...pattern.description.rules })}
      />
      <AssetForm
        fullPath={fullPath}
        release={data?.release}
        onRelease={(tagName) =>
          releaseLazyQuery({ variables: { fullPath, tagName } })
        }
      />
      <Box>
        <Controller
          control={control}
          name="latest"
          render={({ field }) => (
            <FormControlLabel
              checked={!!field.value}
              control={<Checkbox size="small" onChange={onChangeLatest} />}
              label="最新版"
            />
          )}
        />
        <Controller
          control={control}
          name="state"
          render={({ field }) => (
            <FormControlLabel
              checked={field.value === ReleaseState.Preview}
              control={<Checkbox size="small" onChange={onChangeState} />}
              label="预览版"
            />
          )}
        />
      </Box>
      <Button type="submit" variant="contained">
        提交
      </Button>
    </ChunkPaper>
  );
}
