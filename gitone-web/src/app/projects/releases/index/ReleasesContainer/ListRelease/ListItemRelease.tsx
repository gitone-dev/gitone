import { DeleteReleaseInput, Policy, Release, ReleaseState } from "@/generated/types";
import RelativeTime from "@/shared/RelativeTime";
import NewReleasesIcon from "@mui/icons-material/NewReleases";
import Chip from "@mui/material/Chip";
import Link from "@mui/material/Link";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { Link as RouterLink } from "react-router-dom";
import ReleaseMenu from "./ReleaseMenu";

interface Props {
  fullPath: string;
  policy: Policy;
  release: Release;
  onDelete: (input: DeleteReleaseInput) => void;
}

export default function ListItemRelease(props: Props) {
  const { fullPath, policy, release, onDelete } = props;

  return (
    <ListItem
      divider
      secondaryAction={
        <ReleaseMenu
          fullPath={fullPath}
          policy={policy}
          release={release}
          onDelete={onDelete}
        />
      }
    >
      <ListItemIcon>
        <NewReleasesIcon />
      </ListItemIcon>
      <ListItemText>
        <Link
          color="text.primary"
          component={RouterLink}
          to={`/${fullPath}/-/releases/tag/${release.tag?.name}`}
          underline="hover"
          variant="body1"
        >
          {release.title}
        </Link>
        {release.latest && (
          <Chip
            sx={{ ml: 1 }}
            size="small"
            label="最新版"
            color="primary"
            variant="outlined"
          />
        )}
        {release.state == ReleaseState.Preview && (
          <Chip
            sx={{ ml: 1 }}
            size="small"
            label="预览版"
            color="secondary"
            variant="outlined"
          />
        )}
        <Stack direction="row" spacing={1}>
          <Typography color="text.secondary" variant="body2">
            {release.creator?.name} 发布于
          </Typography>
          <Typography color="text.secondary" variant="body2">
            <RelativeTime date={release.createdAt} />
          </Typography>
          <Typography color="text.secondary" variant="body2">
            {release.tag?.name}
          </Typography>
        </Stack>
      </ListItemText>
    </ListItem>
  );
}
