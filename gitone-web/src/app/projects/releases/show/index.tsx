import {
  Action,
  DeleteReleaseInput,
  useDeleteReleaseMutation,
  useReleaseQuery,
} from "@/generated/types";
import ErrorBox from "@/shared/ErrorBox";
import LoadingBox from "@/shared/LoadingBox";
import { useFullPath } from "@/utils/router";
import { useSnackbar } from "notistack";
import { useNavigate } from "react-router-dom";
import ReleasePaper from "./ReleasePaper";

export default function Show() {
  const { fullPath, star: tagName } = useFullPath();
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const { data, loading, error } = useReleaseQuery({
    fetchPolicy: "network-only",
    variables: {
      fullPath,
      tagName,
    },
  });
  const [deleteReleaseMutation] = useDeleteReleaseMutation({
    onCompleted() {
      enqueueSnackbar("删除成功", { variant: "success" });
      navigate(`/${fullPath}/-/releases`);
    },
    onError(error) {
      enqueueSnackbar(error.message, { variant: "error" });
    },
  });

  const release = data?.release;
  const policy = data?.namespacePolicy;

  if (loading) {
    return <LoadingBox />;
  } else if (error) {
    return <ErrorBox message={error.message} />;
  } else if (!release) {
    return <ErrorBox message="查询出错" />;
  } else if (!policy?.actions?.includes(Action.Read)) {
    return <ErrorBox message="无权限" />;
  }

  const onDelete = () => {
    const input: DeleteReleaseInput = { id: release.id };
    deleteReleaseMutation({ variables: { input } });
  };

  return (
    <ReleasePaper
      fullPath={fullPath}
      policy={policy}
      release={release}
      onDelete={onDelete}
    />
  );
}
