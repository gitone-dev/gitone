import { Action, Policy, Release } from "@/generated/types";
import ChunkPaper from "@/shared/ChunkPaper";
import ListAsset from "@/shared/ListAsset";
import RelativeTime from "@/shared/RelativeTime";
import SellIcon from "@mui/icons-material/Sell";
import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";
import Divider from "@mui/material/Divider";
import Link from "@mui/material/Link";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { Link as RouterLink } from "react-router-dom";

interface Props {
  fullPath: string;
  policy: Policy;
  release: Release;
  onDelete: () => void;
}

export default function ReleasePaper(props: Props) {
  const { fullPath, policy, release, onDelete } = props;

  const actions = policy.actions;
  const tagName = release.tag?.name;

  return (
    <ChunkPaper
      primary={release.title || ""}
      action={
        <ButtonGroup size="small">
          <Button
            disabled={!actions.includes(Action.Update)}
            component={RouterLink}
            to={`/${fullPath}/-/releases/edit/${tagName}`}
            state={release}
          >
            编辑
          </Button>
          <Button
            disabled={!actions.includes(Action.Update)}
            onClick={onDelete}
          >
            删除
          </Button>
        </ButtonGroup>
      }
    >
      <Stack direction="row" spacing={1}>
        <Typography color="text.secondary" variant="body2">
          {release.creator?.name} 发布于
        </Typography>
        <Typography color="text.secondary" variant="body2">
          <RelativeTime date={release.createdAt} />
        </Typography>
        <>
          <SellIcon fontSize="small" />
          <Link
            color="text.secondary"
            component={RouterLink}
            to={`/${fullPath}/-/tags/${tagName}`}
            underline="hover"
            variant="body2"
          >
            {tagName}
          </Link>
        </>
      </Stack>
      <Typography sx={{ whiteSpace: "pre-wrap", mt: 2 }} variant="body2">
        {release.description}
      </Typography>
      <Divider sx={{ my: 2 }} />
      <ListAsset
        fullPath={fullPath}
        tag={release.tag}
        edges={release.assets?.edges}
      />
    </ChunkPaper>
  );
}
