import { Action, useReleaseQuery } from "@/generated/types";
import ErrorBox from "@/shared/ErrorBox";
import LoadingBox from "@/shared/LoadingBox";
import { useFullPath } from "@/utils/router";
import Form from "./Form";

export default function Edit() {
  const { fullPath, star: tagName } = useFullPath();
  const { data, loading, error } = useReleaseQuery({
    variables: { fullPath, tagName },
  });

  const policy = data?.namespacePolicy;
  const release = data?.release;

  if (loading) {
    return <LoadingBox />;
  } else if (error) {
    return <ErrorBox message={error.message} />;
  } else if (!release) {
    return <ErrorBox message="查询出错" />;
  } else if (!policy?.actions?.includes(Action.Update)) {
    return <ErrorBox message="无权限" />;
  }

  return <Form fullPath={fullPath} release={release} />;
}
