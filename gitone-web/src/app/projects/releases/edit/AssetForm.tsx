import { uploadAsset } from "@/client";
import {
  AssetEdge,
  DeleteAssetInput,
  Release,
  useCreateAssetMutation,
  useDeleteAssetMutation,
} from "@/generated/types";
import { ListItemAsset } from "@/shared/ListAsset";
import { useApolloClient } from "@apollo/client";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutlineOutlined";
import DeleteIcon from "@mui/icons-material/Delete";
import FormHelperText from "@mui/material/FormHelperText";
import FormLabel from "@mui/material/FormLabel";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import List from "@mui/material/List";
import Stack from "@mui/material/Stack";
import { useSnackbar } from "notistack";
import React from "react";

const style = {
  mt: 1,
  py: 0,
  width: "100%",
  borderRadius: 2,
  border: "1px solid",
  borderColor: "divider",
  backgroundColor: "background.paper",
};

interface Props {
  release: Release;
}

export default function AssetForm(props: Props) {
  const { release } = props;
  const { enqueueSnackbar } = useSnackbar();

  const { cache } = useApolloClient();
  const [createAssetMutation, { loading }] = useCreateAssetMutation({});
  const [deleteAssetMutation] = useDeleteAssetMutation({
    update(cache, { data: result }) {
      const asset = result?.payload?.asset;
      if (!asset) return;

      cache.modify({
        id: cache.identify({ __typename: "Release", id: release.id }),
        fields: {
          assets(existingRefs = {}, { readField }) {
            const edges = existingRefs.edges?.filter(
              (edge: AssetEdge) => readField("id", edge.node) !== asset.id
            );
            return { ...existingRefs, edges };
          },
        },
      });
    },
    onCompleted() {
      enqueueSnackbar("删除成功", { variant: "success" });
    },
    onError(error) {
      enqueueSnackbar(error.message, { variant: "error" });
    },
  });

  const onCreateAsset = (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files;
    if (!files || files.length !== 1) return;
    const file = files[0];

    createAssetMutation({
      variables: {
        input: {
          releaseId: release.id,
          name: file.name,
        },
      },
      onCompleted(data) {
        const asset = data.payload?.asset;
        if (!asset?.id) return;

        uploadAsset(asset.id, file)
          .then((response) => {
            if (!response.ok) {
              throw new Error(`HTTP error! Status: ${response.status}`);
            }

            cache.modify({
              id: cache.identify({ __typename: "Release", id: release.id }),
              fields: {
                assets(existingRefs = {}, { readField, toReference }) {
                  if (
                    existingRefs.edges?.some(
                      (edge: AssetEdge) =>
                        readField("id", edge.node) === asset.id
                    )
                  ) {
                    return existingRefs;
                  }

                  return {
                    ...existingRefs,
                    edges: [
                      ...existingRefs.edges,
                      {
                        __typename: "AssetEdge",
                        node: toReference(asset),
                      },
                    ],
                  };
                },
              },
            });
            enqueueSnackbar("文件上传成功", { variant: "success" });
          })
          .catch((error: Error) => {
            enqueueSnackbar(error.message, { variant: "error" });
          });
      },
      onError(error) {
        enqueueSnackbar(error.message, { variant: "error" });
      },
    });
  };

  const onDelete = (input: DeleteAssetInput) => {
    deleteAssetMutation({ variables: { input } });
  };

  return (
    <>
      <Stack direction="row" alignItems="center">
        <FormLabel>资源</FormLabel>
        <IconButton
          disabled={loading}
          component="label"
          tabIndex={-1}
          size="small"
        >
          <AddCircleOutlineIcon fontSize="small" />
          <Input
            type="file"
            sx={{ display: "none" }}
            onChange={onCreateAsset}
          />
        </IconButton>
        <FormHelperText>变更立刻生效</FormHelperText>
      </Stack>
      <List dense sx={style}>
        {release.assets?.edges?.map((edge) => (
          <ListItemAsset
            key={edge.cursor}
            name={edge.node.name}
            size={edge.node.size}
            date={edge.node.createdAt}
            secondaryAction={
              <IconButton
                size="small"
                onClick={() => onDelete({ id: edge.node.id })}
              >
                <DeleteIcon fontSize="small" />
              </IconButton>
            }
          />
        ))}
      </List>
    </>
  );
}
