import {
  Release,
  ReleaseState,
  TagEdge,
  TagFilter,
  UpdateReleaseInput,
  useTagsLazyQuery,
  useUpdateReleaseMutation,
} from "@/generated/types";
import ChunkPaper from "@/shared/ChunkPaper";
import { release as pattern } from "@/utils/regex";
import Autocomplete from "@mui/material/Autocomplete";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import debounce from "@mui/material/utils/debounce";
import { Maybe } from "graphql/jsutils/Maybe";
import { useSnackbar } from "notistack";
import { useMemo, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import AssetForm from "./AssetForm";

interface Props {
  fullPath: string;
  release: Release;
}

export default function Form(props: Props) {
  const { fullPath, release } = props;
  const tagEdge: TagEdge | null = release.tag
    ? { cursor: release.tag.id, node: release.tag }
    : null;
  const [options, setOptions] = useState<TagEdge[]>(tagEdge ? [tagEdge] : []);
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const {
    control,
    formState: { errors },
    handleSubmit,
    register,
    setValue,
  } = useForm<UpdateReleaseInput>({
    defaultValues: {
      tagName: release.tag?.name,
      title: release.title,
      description: release.description,
      state: release.state,
      latest: release.latest,
    },
  });

  const [tagsLazyQuery, { loading }] = useTagsLazyQuery({
    fetchPolicy: "network-only",
  });

  const [updateReleaseMutation] = useUpdateReleaseMutation({
    onCompleted(data) {
      const release = data?.payload?.release;
      if (!release?.id) return;

      enqueueSnackbar("更新成功", { variant: "success" });
      navigate(`/${fullPath}/-/releases/tag/${release.tag?.name}`, {
        replace: true,
      });
    },
    onError(error) {
      enqueueSnackbar(error.message, { variant: "error" });
    },
  });

  const tagsQuery = useMemo(
    () =>
      debounce((query: string) => {
        const filterBy: TagFilter = { query };
        tagsLazyQuery({
          variables: { fullPath, first: 20, filterBy },
          onCompleted(data) {
            setOptions(data.repository.tags?.edges || []);
          },
        });
      }, 500),
    [tagsLazyQuery]
  );

  const onChangeTagName = (_event: unknown, edge: Maybe<TagEdge>) => {
    if (!edge?.node.name) return;

    setValue("tagName", edge.node.name);
  };

  const onInputChange = (_event: unknown, value: string) => {
    tagsQuery(value);
  };

  const onChangeState = (_event: unknown, checked: boolean) => {
    if (checked) {
      setValue("latest", false);
      setValue("state", ReleaseState.Preview);
    } else if (!release.latest) {
      setValue("state", ReleaseState.Published);
    }
  };

  const onChangeLatest = (_event: unknown, checked: boolean) => {
    if (checked) {
      setValue("latest", true);
      setValue("state", ReleaseState.Published);
    } else if (!release.latest) {
      setValue("latest", false);
    }
  };

  const onUpdate = handleSubmit((input: UpdateReleaseInput) => {
    updateReleaseMutation({ variables: { input } });
  });

  return (
    <ChunkPaper primary="编辑" component="form" onSubmit={onUpdate}>
      <TextField
        sx={{ display: "none" }}
        {...register("id", { value: release.id })}
      />
      <Autocomplete
        defaultValue={tagEdge}
        fullWidth
        sx={{ mt: 2 }}
        loading={loading}
        getOptionLabel={(option) => option.node.name || ""}
        filterOptions={(x) => x}
        options={options}
        loadingText="加载中"
        autoComplete
        includeInputInList
        noOptionsText="无"
        onChange={onChangeTagName}
        isOptionEqualToValue={(option, value) =>
          option.node.name === value.node.name
        }
        onInputChange={onInputChange}
        renderInput={(params) => (
          <TextField {...params} required size="small" label="标签" />
        )}
        renderOption={(props, option) => {
          return (
            <ListItem {...props}>
              <ListItemText primary={option.node.name} />
            </ListItem>
          );
        }}
      />
      <TextField
        error={Boolean(errors.title)}
        fullWidth
        helperText={errors.title?.message || pattern.title.helper}
        label="标题"
        margin="dense"
        required
        size="small"
        {...register("title", { ...pattern.title.rules })}
      />
      <TextField
        error={Boolean(errors.description)}
        fullWidth
        helperText={errors.description?.message || pattern.description.helper}
        label="描述"
        multiline
        minRows={5}
        maxRows={5}
        {...register("description", { ...pattern.description.rules })}
      />
      <AssetForm release={release} />
      <Box>
        <Controller
          control={control}
          name="latest"
          render={({ field }) => (
            <FormControlLabel
              checked={!!field.value}
              control={<Checkbox size="small" onChange={onChangeLatest} />}
              label="最新版"
            />
          )}
        />
        <Controller
          control={control}
          name="state"
          render={({ field }) => (
            <FormControlLabel
              checked={field.value === ReleaseState.Preview}
              control={<Checkbox size="small" onChange={onChangeState} />}
              label="预览版"
            />
          )}
        />
      </Box>
      <Stack direction="row" spacing={2} sx={{ mt: 1 }}>
        <Button type="submit" variant="contained">
          提交
        </Button>
        <Button
          component={RouterLink}
          to={`/${fullPath}/-/releases/tag/${release.tag?.name}`}
        >
          取消
        </Button>
      </Stack>
    </ChunkPaper>
  );
}
