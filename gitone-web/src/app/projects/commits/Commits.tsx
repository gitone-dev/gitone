import { useRevisionPathQuery } from "@/generated/types";
import { BreadcrumbItems } from "@/layout/Breadcrumbs";
import ChunkPaper from "@/shared/ChunkPaper";
import ErrorBox from "@/shared/ErrorBox";
import LoadingBox from "@/shared/LoadingBox";
import RevisionPath from "@/shared/RevisionPath";
import CommitsContainer from "./CommitsContainer";

const breadcrumbItems = (
  fullPath: string,
  revision: string,
  path: string
): BreadcrumbItems => {
  const items = [];
  const paths = path.split("/");

  for (let i = 1; i < paths.length; i++) {
    const path = paths.slice(0, i).join("/");
    items.push({
      to: `/${fullPath}/-/commits/${revision}/${path}`,
      text: paths[i - 1],
    });
  }
  items.push({
    to: `/${fullPath}/-/commits/${revision}/${path}`,
    text: paths[paths.length - 1],
  });

  return {
    [`/${fullPath}/-/commits/${revision}/${path}`]: [...items],
  };
};

interface Props {
  fullPath: string;
  star: string | null | undefined;
}

export default function Commits(props: Props) {
  const { fullPath, star } = props;
  const { data, loading, error } = useRevisionPathQuery({
    variables: { fullPath, revisionPath: star },
  });

  const revisionPath = data?.repository.revisionPath;

  if (loading) {
    return <LoadingBox />;
  } else if (error) {
    return <ErrorBox message={error.message} />;
  } else if (!revisionPath) {
    return <ErrorBox message="查询错误" />;
  }

  const { revision, path } = revisionPath;

  return (
    <>
      <RevisionPath
        fullPath={fullPath}
        type="commits"
        revision={revision}
        path={path}
        breadcrumbItems={breadcrumbItems(fullPath, revision, path)}
      />
      <ChunkPaper primary="提交列表">
        <CommitsContainer fullPath={fullPath} revisionPath={revisionPath} />
      </ChunkPaper>
    </>
  );
}
