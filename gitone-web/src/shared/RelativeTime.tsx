import Box from "@mui/material/Box";
import dayjs from "./dayts";

interface Props {
  date: string;
}

export default function RelativeTime(props: Props) {
  const day = dayjs(props.date);

  return (
    <Box component="span" title={props.date} sx={{ whiteSpace: "nowrap" }}>
      {day.fromNow()}
    </Box>
  );
}
