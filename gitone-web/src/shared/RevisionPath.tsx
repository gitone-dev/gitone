import CodePopper from "@/app/projects/tree/CodePopper";
import Breadcrumbs, { BreadcrumbItems } from "@/layout/Breadcrumbs";
import RefSwitcher from "@/shared/RefSwitcher";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import IconButton from "@mui/material/IconButton";
import Stack from "@mui/material/Stack";
import copy from "copy-to-clipboard";
import { useSnackbar } from "notistack";
import { PathType } from "./RefSwitcher/RefSwitcher";

interface Props {
  fullPath: string;
  type: PathType;
  revision: string;
  path: string;
  breadcrumbItems: BreadcrumbItems;
}

export default function RevisionPath(props: Props) {
  const { fullPath, type, revision, path, breadcrumbItems } = props;

  const { enqueueSnackbar } = useSnackbar();

  const onClick = () => {
    if (!path) return;
    copy(path);
    enqueueSnackbar(`已复制：${path}`, { variant: "info" });
  };

  const getPathname = (
    type: string,
    revision: string,
    path: string
  ): string => {
    return `/${fullPath}/-/${type}/${revision}/${path}`;
  };
  const isTreeRoot = path === "" && type === "tree";

  return (
    <>
      <Stack
        direction="row"
        spacing={1}
        alignItems="center"
        justifyContent={isTreeRoot ? "space-between" : "flex-start"}
        sx={{
          backgroundColor: "white",
          pt: 2,
          position: "sticky",
          top: 60,
          zIndex: 1,
        }}
      >
        <RefSwitcher
          fullPath={fullPath}
          type={type}
          revision={revision}
          path={path}
          getPathname={getPathname}
        />
        <Breadcrumbs items={breadcrumbItems} />
        {path && (
          <IconButton onClick={onClick}>
            <ContentCopyIcon fontSize="small" />
          </IconButton>
        )}
        {isTreeRoot && <CodePopper fullPath={fullPath} revision={revision} />}
      </Stack>
    </>
  );
}
