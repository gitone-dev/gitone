import RelativeTime from "@/shared/RelativeTime";
import { humanReadable } from "@/utils/size";
import BusinessCenterIcon from "@mui/icons-material/BusinessCenterOutlined";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Stack from "@mui/material/Stack";

interface Props {
  name: string;
  date: string;
  size?: number;
  icon?: React.ReactNode;
  href?: string;
  secondaryAction?: React.ReactNode;
}

export default function ListItemAsset(props: Props) {
  const { icon, name, href, size, date, secondaryAction } = props;

  return (
    <ListItem divider secondaryAction={secondaryAction}>
      <ListItemIcon sx={{ minWidth: 32 }}>
        {icon || <BusinessCenterIcon />}
      </ListItemIcon>
      <ListItemText>
        <Stack direction="row" spacing={2} justifyContent="space-between">
          {href ? (
            <Link
              flexGrow={1}
              color="text.primary"
              href={href}
              underline="hover"
            >
              {name}
            </Link>
          ) : (
            <Box flexGrow={1}>{name}</Box>
          )}
          {size && <Box>{humanReadable(size)}</Box>}
          <Box width={80} textAlign="right">
            <RelativeTime date={date} />
          </Box>
        </Stack>
      </ListItemText>
    </ListItem>
  );
}
