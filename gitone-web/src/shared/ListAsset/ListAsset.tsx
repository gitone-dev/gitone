import { AssetEdge, Tag } from "@/generated/types";
import FolderZipIcon from "@mui/icons-material/FolderZipOutlined";
import FormLabel from "@mui/material/FormLabel";
import List from "@mui/material/List";
import ListItemAsset from "./ListItemAsset";

const style = {
  mt: 1,
  py: 0,
  width: "100%",
  borderRadius: 2,
  border: "1px solid",
  borderColor: "divider",
  backgroundColor: "background.paper",
};

interface Props {
  fullPath: string;
  tag: Tag | null | undefined;
  edges: Array<AssetEdge> | null | undefined;
  secondaryAction?: React.ReactNode;
}

export default function ListAsset(props: Props) {
  const { fullPath, tag, edges } = props;

  return (
    <>
      <FormLabel>资源</FormLabel>
      <List dense sx={style}>
        <ListItemAsset
          icon={<FolderZipIcon />}
          name="源码（.zip）"
          href={`/${fullPath}/-/archive/${tag?.name}.zip`}
          date={tag?.commit?.committer?.date}
        />
        <ListItemAsset
          icon={<FolderZipIcon />}
          name="源码（.tar.gz）"
          href={`/${fullPath}/-/archive/${tag?.name}.tar.gz`}
          date={tag?.commit?.committer?.date}
        />
        {edges?.map((edge) => (
          <ListItemAsset
            key={edge.cursor}
            name={edge.node.name}
            href={`/${fullPath}/-/releases/download/${tag?.name}/${edge.node.name}`}
            size={edge.node.size}
            date={edge.node.createdAt}
          />
        ))}
      </List>
    </>
  );
}
