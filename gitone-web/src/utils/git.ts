import { Blob } from "@/generated/types";

const SHA_ABBR_LENGTH = 8;

function pathEqual(left: string, right: string): boolean {
  if (left.endsWith("/")) left = left.substring(0, left.length - 1);
  if (right.endsWith("/")) right = right.substring(0, right.length - 1);
  return left === right;
}

function isBinary(blob: Blob): boolean {
  return !!blob.contentType?.startsWith("application/");
}

function isText(blob: Blob): boolean {
  return !!blob.contentType?.startsWith("text/");
}

function isImage(blob: Blob): boolean {
  return !!blob.contentType?.startsWith("image/");
}

export { SHA_ABBR_LENGTH, isBinary, isImage, isText, pathEqual };
