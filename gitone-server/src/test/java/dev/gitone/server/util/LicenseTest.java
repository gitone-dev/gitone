package dev.gitone.server.util;

import dev.gitone.server.models.git.License;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.Map;

class LicenseTest {
    @Test
    void nameScore() {
        Map<String, Double> expected = new LinkedHashMap<>();
        expected.put("LICENSE", 1.00);
        expected.put("License", 1.00);
        expected.put("license", 1.00);
        expected.put("LICENCE", 1.00);
        expected.put("Licence", 1.00);
        expected.put("licence", 1.00);

        expected.put("LICENSE.md", 0.95);
        expected.put("License.markdown", 0.95);
        expected.put("license.txt", 0.95);
        expected.put("licence.html", 0.95);

        expected.put("COPYING", 0.90);
        expected.put("Copying", 0.90);
        expected.put("copying", 0.90);
        expected.put("COPYRIGHT", 0.90);
        expected.put("Copyright", 0.90);
        expected.put("copyright", 0.90);

        expected.put("COPYING.md",  0.85);
        expected.put("Copying.markdown", 0.85);
        expected.put("copying.txt", 0.85);
        expected.put("copyright.html", 0.85);

        expected.put("LICENSE.textile", 0.80);
        expected.put("LICENCE.adoc", 0.80);

        expected.put("COPYING.textile",  0.75);
        expected.put("COPYRIGHT.adoc", 0.75);

        expected.put("LICENSE-MIT", 0.70);
        expected.put("LICENSE_MIT", 0.70);
        expected.put("LICENCE-MIT.adoc", 0.70);
        expected.put("LICENCE_MIT.adoc", 0.70);

        expected.put("COPYING-MIT", 0.65);
        expected.put("COPYING_MIT", 0.65);
        expected.put("COPYRIGHT-MIT.adoc", 0.65);
        expected.put("COPYRIGHT_MIT.adoc", 0.65);

        expected.put("MIT-LICENSE", 0.60);
        expected.put("MIT_LICENSE", 0.60);
        expected.put("MIT-LICENCE.adoc", 0.60);
        expected.put("MIT_LICENCE.adoc", 0.60);
        expected.put("PREFIX-LICENCE-SUFFIX", 0.60);

        expected.put("MIT-COPYING", 0.55);
        expected.put("MIT_COPYING", 0.55);
        expected.put("MIT-COPYRIGHT.adoc", 0.55);
        expected.put("MIT_COPYRIGHT.adoc", 0.55);
        expected.put("PREFIX-COPYRIGHT-SUFFIX.adoc", 0.55);

        expected.put("l", 0.00);
        expected.put("li", 0.00);
        expected.put("\nlicense", 0.00);
        expected.put("license\n", 0.00);

        for (Map.Entry<String, Double> entry: expected.entrySet()) {
            System.out.printf("%s, %f\n", entry.getKey(), entry.getValue());
            Assertions.assertEquals(entry.getValue(), License.nameScore(entry.getKey()));
        }
    }
}
