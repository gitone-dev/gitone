CREATE TABLE assets (
  id serial PRIMARY KEY,
  created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
  release_id integer NOT NULL,
  name character varying NOT NULL,
  content_type character varying,
  size bigint DEFAULT 0 NOT NULL,
  state integer DEFAULT -1 NOT NULL,
  sha256 bytea,
  created_by_id integer NOT NULL
);

CREATE UNIQUE INDEX index_assets_on_release_id_and_name ON assets USING btree (release_id, name);
