package dev.gitone.server.util;

import org.springframework.http.MediaType;

import java.util.Set;

public class Markup {

    public static final MediaType TEXT_ASCIIDOC = new MediaType("text", "x-asciidoc");

    public final static MediaType TEXT_MARKDOWN = new MediaType("text", "x-markdown");

    public static final Set<String> ASCIIDOC_EXTENSIONS = Set.of("adoc", "asciidoc");

    public static final Set<String> MARKDOWN_EXTENSIONS = Set.of("md", "markdown");

    public static boolean isMarkup(String extension) {
        return isAsciidoc(extension) || isMarkdown(extension);
    }

    public static boolean isAsciidoc(String extension) {
        return ASCIIDOC_EXTENSIONS.contains(extension);
    }

    public static boolean isMarkdown(String extension) {
        return MARKDOWN_EXTENSIONS.contains(extension);
    }

    public static MediaType mediaType(String extension, MediaType defaultValue) {
        if (isAsciidoc(extension)) {
            return TEXT_ASCIIDOC;
        } else if (isMarkdown(extension)) {
            return TEXT_MARKDOWN;
        } else {
            return defaultValue;
        }
    }
}
