package dev.gitone.server.util.detection;

import com.google.common.primitives.Bytes;
import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

public class Detection {

    private static final byte[][] prefixBins = {
            // application/postscript
            { '%', '!', 'P', 'S', '-', 'A', 'd', 'o', 'b', 'e', '-' },
            // image/png
            { (byte) 0x89, 'P', 'N', 'G', 0x0D, 0x0A, 0x1A, 0x0A },
            // image/gif
            { 'G', 'I', 'F', '8', '7', 'a' },
            // image/gif
            { 'G', 'I', 'F', '8', '9', 'a' },
            // application/pdf
            { '%', 'P', 'D', 'F', '-' },
            // UTF-32BE
            { 0, 0, (byte) 0xfe, (byte) 0xff},
            // UTF-32LE
            { (byte) 0xff, (byte) 0xfe, 0, 0 },
            // image/jpeg
            { (byte) 0xFF, (byte) 0xD8, (byte) 0xFF },
            // UTF-16BE
            { (byte) 0xfe, (byte) 0xff },
            // UTF-16LE
            { (byte) 0xff, (byte) 0xfe },
    };

    public static CharsetMatch detect(byte[] data) {
        if (detectBinary(data))
            return null;

        CharsetDetector detector = new CharsetDetector();
        detector.setText(data);
        return detector.detect();
    }

    public static boolean detectBinary(byte[] data) {
        for (byte[] prefix : prefixBins) {
            if (hasPrefix(prefix, data))
                return true;
        }

        return Bytes.contains(data, (byte) 0);
    }

    public static boolean detectText(byte[] data) {
        return !detectBinary(data);
    }

    private static boolean hasPrefix(byte[] prefix, byte[] data) {
        if (prefix.length > data.length)
            return false;

        for (int i = 0; i < prefix.length; i++) {
            if (prefix[i] != data[i])
                return false;
        }
        return true;
    }
}
