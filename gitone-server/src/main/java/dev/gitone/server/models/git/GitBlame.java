package dev.gitone.server.models.git;

import dev.gitone.server.entities.Node;
import lombok.Getter;
import org.eclipse.jgit.api.BlameCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.blame.BlameResult;
import org.eclipse.jgit.util.sha1.SHA1;

import java.io.IOException;

public class GitBlame implements Node<String> {

    public static final String TYPE = "Blame";

    @Getter
    private final GitRepository gitRepository;

    private final GitCommit gitCommit;

    @Getter
    private final BlameResult blame;

    @Getter
    private final String path;

    public GitBlame(GitRepository gitRepository, GitCommit gitCommit, String path, BlameResult blameResult) {
        this.gitRepository = gitRepository;
        this.gitCommit = gitCommit;
        this.path = path;
        this.blame= blameResult;
    }

    @Override
    public String getId() {
        SHA1 sha1 = SHA1.newInstance();
        sha1.update(path.getBytes());
        return sha1.toObjectId().getName();
    }

    public String getName() {
        return Util.filename(path);
    }

    public GitCommit getCommit(int idx) {
        return new GitCommit(gitRepository, blame.getSourceCommit(idx));
    }

    public String getSourcePath(int idx) {
        return blame.getSourcePath(idx);
    }

    public int size() {
        return blame.getResultContents().size();
    }

    public static GitBlame find(GitRepository gitRepository, String revision, String path) throws IOException {
        GitCommit gitCommit = GitCommit.find(gitRepository, revision);
        if (gitCommit == null) return null;

        GitBlob gitBlob = GitBlob.find(gitRepository, gitCommit, path);
        if (gitBlob == null) return null;

        BlameCommand command = new BlameCommand(gitRepository.repository);
        command.setStartCommit(gitCommit.revCommit);
        command.setFilePath(path);
        try {
            BlameResult result = command.call();
            return new GitBlame(gitRepository, gitCommit, path, result);
        } catch (GitAPIException e) {
            throw new RuntimeException(e);
        }
    }
}
