package dev.gitone.server.models.git;

import dev.gitone.server.entities.Node;
import lombok.Data;
import org.eclipse.jgit.util.sha1.SHA1;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

@Data
public class License implements Node<String> {

    public static final String TYPE = "License";

    // https://github.com/licensee/licensee/blob/main/lib/licensee/project_files/license_file.rb
    private static final Map<Pattern, Double> FILENAME_REGEXES = new LinkedHashMap<>();

    static {
        // LICENSE
        FILENAME_REGEXES.put(Pattern.compile(
                "\\A(?i-mx:(un)?licen[sc]e)\\z"), 1.00);
        // LICENSE.md
        FILENAME_REGEXES.put(Pattern.compile(
                "\\A(?i-mx:(un)?licen[sc]e)(?-mix:\\.(?-mix:md|markdown|txt|html)\\z)\\z"), 0.95);
        // COPYING
        FILENAME_REGEXES.put(Pattern.compile(
                "\\A(?i-mx:copy(ing|right))\\z"), 0.90);
        // COPYING.md
        FILENAME_REGEXES.put( Pattern.compile(
                "\\A(?i-mx:copy(ing|right))(?-mix:\\.(?-mix:md|markdown|txt|html)\\z)\\z"), 0.85);
        // LICENSE.textile
        FILENAME_REGEXES.put(Pattern.compile(
                "\\A(?i-mx:(un)?licen[sc]e)(?i-mx:\\.(?!spdx|header)[^./]+\\z)\\z"), 0.80);
        // COPYING.textile
        FILENAME_REGEXES.put(Pattern.compile(
                "\\A(?i-mx:copy(ing|right))(?i-mx:\\.[^./]+\\z)\\z"), 0.75);
        // LICENSE-MIT
        FILENAME_REGEXES.put(Pattern.compile(
                "\\A(?i-mx:(un)?licen[sc]e)[-_][^.]*(?i-mx:\\.(?!xml|go|gemspec)[^./]+\\z)?\\z"), 0.70);
        // COPYING-MIT
        FILENAME_REGEXES.put(Pattern.compile(
                "\\A(?i-mx:copy(ing|right))[-_][^.]*(?i-mx:\\.(?!xml|go|gemspec)[^./]+\\z)?\\z"), 0.65);
        // MIT-LICENSE-MIT
        FILENAME_REGEXES.put(Pattern.compile(
                "\\A\\w+[-_](?i-mx:(un)?licen[sc]e)[^.]*(?i-mx:\\.(?!xml|go|gemspec)[^./]+\\z)?\\z"), 0.60);
        // MIT-COPYING
        FILENAME_REGEXES.put(Pattern.compile(
                "\\A\\w+[-_](?i-mx:copy(ing|right))[^.]*(?i-mx:\\.(?!xml|go|gemspec)[^./]+\\z)?\\z"), 0.55 );
        // Catch all
        FILENAME_REGEXES.put(Pattern.compile(""), 0.00);
    }

    public static Double nameScore(String filename) {
        for (Map.Entry<Pattern, Double> entry: FILENAME_REGEXES.entrySet()) {
            if (entry.getKey().matcher(filename).matches())
                return entry.getValue();
        }
        return 0.0;
    }

    public static List<License> findAll(GitRepository gitRepository) throws IOException {
        List<License> result = new ArrayList<>();

        GitBranch defaultBranch = gitRepository.defaultBranch();
        if (defaultBranch == null) return result;

        GitTree gitTree = GitTree.find(gitRepository, defaultBranch.getName(), "");
        if (gitTree == null) return result;

        for (GitTreeEntry entry : gitTree.getEntries()) {
            if (!entry.isBlob()) continue;
            Double score = nameScore(entry.getName());
            if (score < 0.01) continue;
            result.add(new License(entry.getPath(), score));
        }
        result.sort(Comparator.comparingDouble(l -> -l.score));
        return result;
    }

    private final String path;

    private final double score;

    @Override
    public String getId() {
        SHA1 sha1 = SHA1.newInstance();
        sha1.update(path.getBytes());
        return sha1.toObjectId().getName();
    }

    public License(String path, double score) {
        this.path = path;
        this.score = score;
    }
}
