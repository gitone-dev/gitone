package dev.gitone.server.models.git;

import dev.gitone.server.entities.Node;
import dev.gitone.server.util.Markup;
import dev.gitone.server.util.detection.Detection;
import lombok.Getter;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.jgit.lib.FileMode;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.eclipse.jgit.util.sha1.SHA1;
import org.springframework.http.MediaType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class GitBlob implements Node<String> {

    public static final String TYPE = "Blob";

    private static final int SIZE_LIMIT = 4 * 1024 * 1024;

    private final GitRepository gitRepository;

    private final GitCommit gitCommit;

    private ObjectLoader loader;

    @Getter
    private final String path;

    private final ObjectId sha;

    private final FileMode mode;

    private Long size;

    private byte[] data;

    private Boolean viewable;

    private MediaType mediaType;

    public GitBlob(GitRepository gitRepository, GitCommit gitCommit, String path, ObjectId sha, FileMode mode) {
        this.gitRepository = gitRepository;
        this.gitCommit = gitCommit;
        this.path = path;
        this.sha = sha;
        this.mode = mode;
    }

    @Override
    public String getId() {
        SHA1 sha1 = SHA1.newInstance();
        sha1.update(path.getBytes());
        return String.format("%s:%s:%s", gitRepository.getId(), gitCommit.getSha(), sha1.toObjectId().getName());
    }

    public String getSha() {
        return sha.name();
    }

    public String getName() {
        return Util.filename(path);
    }

    private String getExtension() {
        return FilenameUtils.getExtension(path);
    }

    public int getMode() {
        return mode.getBits();
    }

    public Long getSize() throws IOException {
        if (size != null) return size;

        this.size = isFile() ? loader().getSize() : 0L;
        return this.size;
    }

    public byte[] getData() throws IOException {
        if (data != null) return data;

        if (isFile()) {
            this.data = loader().getBytes(SIZE_LIMIT);
            this.viewable = getSize() <= SIZE_LIMIT && Detection.detectText(data);
        } else {
            this.data = new byte[0];
            this.viewable = false;
        }
        return data;
    }

    public Boolean isViewable() throws IOException {
        if (viewable == null) getData();
        return viewable;
    }

    public Boolean isImage() throws IOException {
        return "image".equals(mediaType().getType());
    }

    public Boolean isMarkup() throws IOException {
        if (isImage()) return false;
        if (!isViewable()) return false;

        return Markup.isMarkup(getExtension());
    }

    public String getContentType() throws IOException {
        return Markup.mediaType(getExtension(), mediaType()).toString();
    }

    public MediaType mediaType() throws IOException {
        if (mediaType != null) return mediaType;

        String contentType = Files.probeContentType(Path.of(getPath()));
        if (contentType != null) {
            this.mediaType = MediaType.valueOf(contentType);
            if ("image".equals(mediaType.getType()))
                return mediaType;
            if ("text".equals(mediaType.getType()))
                return this.mediaType = MediaType.TEXT_PLAIN;
        }

        if (isViewable()) {
            return this.mediaType = MediaType.TEXT_PLAIN;
        } else {
            return this.mediaType = MediaType.APPLICATION_OCTET_STREAM;
        }
    }

    public InputStream inputStream() throws IOException {
        if (data != null && getSize() <= SIZE_LIMIT) {
            return new ByteArrayInputStream(data);
        }
        return loader().openStream();
    }

    public Integer linesCount() throws IOException {
        if (!isViewable()) return -1;
        if (data.length == 0) return 0;

        int count = 0;
        byte[] data = getData();;
        for (byte datum : data) {
            if (datum == '\n') count += 1;
        }
        if (data[data.length - 1] != '\n') {
            count += 1;
        }
        return count;
    }

    private ObjectLoader loader() throws IOException {
        if (loader == null)
            loader = gitRepository.repository.open(sha);
        return loader;
    }

    private boolean isFile() {
        return (mode.getBits() & FileMode.TYPE_MASK) == FileMode.TYPE_FILE;
    }

    public static GitBlob find(GitRepository gitRepository, String revision, String path) throws IOException {
        GitCommit gitCommit = GitCommit.find(gitRepository, revision);
        if (gitCommit == null) return null;

        return find(gitRepository, gitCommit, path);
    }

    public static GitBlob find(GitRepository gitRepository, GitCommit gitCommit, String path) throws IOException {
        try (TreeWalk walk = new TreeWalk(gitRepository.repository)) {
            int nth = walk.addTree(gitCommit.revCommit.getTree());
            walk.setRecursive(true);
            walk.setFilter(PathFilter.create(path));

            if (!walk.next()) return null;
            FileMode mode = walk.getFileMode(nth);
            ObjectId sha = walk.getObjectId(nth);
            return new GitBlob(gitRepository, gitCommit, path, sha, mode);
        }
    }
}
