package dev.gitone.server.models;

import dev.gitone.server.models.git.GitCommit;
import lombok.Data;

@Data
public class BlameLine {

    private GitCommit commit;

    private String sourcePath;

    private Integer left;

    private Integer right;

    public BlameLine(Integer left, Integer right) {
        this.left = left;
        this.right = right;
    }
}
