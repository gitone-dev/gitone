package dev.gitone.server.services.node;

import dev.gitone.node.markup.MarkupGrpc;
import dev.gitone.node.markup.MarkupRequest;
import dev.gitone.node.markup.MarkupResponse;
import dev.gitone.server.config.CustomProperties;
import io.grpc.Grpc;
import io.grpc.InsecureChannelCredentials;
import io.grpc.ManagedChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MarkupService {

    private final MarkupGrpc.MarkupBlockingStub blockingStub;

    @Autowired
    public MarkupService(CustomProperties properties) {
        CustomProperties.Node node = properties.getNode();
        String target = String.format("%s:%d", node.getHost(), node.getPort());
        ManagedChannel channel = Grpc.newChannelBuilder(target, InsecureChannelCredentials.create()).build();
        this.blockingStub = MarkupGrpc.newBlockingStub(channel);
    }


    public MarkupResponse markup(MarkupRequest request) {
        return blockingStub.markup(request);
    }
}
