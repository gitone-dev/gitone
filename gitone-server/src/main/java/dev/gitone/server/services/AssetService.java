package dev.gitone.server.services;

import dev.gitone.server.ViewerContext;
import dev.gitone.server.config.CustomProperties;
import dev.gitone.server.config.exception.NotFound;
import dev.gitone.server.controllers.assets.inputs.CreateAssetInput;
import dev.gitone.server.controllers.assets.inputs.DeleteAssetInput;
import dev.gitone.server.controllers.assets.inputs.UpdateAssetInput;
import dev.gitone.server.daos.AssetDao;
import dev.gitone.server.daos.ProjectDao;
import dev.gitone.server.daos.ReleaseDao;
import dev.gitone.server.entities.AssetEntity;
import dev.gitone.server.entities.AssetState;
import dev.gitone.server.entities.ProjectEntity;
import dev.gitone.server.entities.ReleaseEntity;
import dev.gitone.server.policies.Action;
import dev.gitone.server.policies.NamespacePolicy;
import lombok.AllArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@AllArgsConstructor
@Service
public class AssetService extends ViewerContext {

    private static final int MB = 1024 * 1024;

    private CustomProperties properties;

    private final ProjectDao projectDao;

    private final ReleaseDao releaseDao;

    private final NamespacePolicy namespacePolicy;

    private final AssetDao assetDao;

    public AssetEntity create(CreateAssetInput input) {
        AssetEntity assetEntity = input.entity();

        ReleaseEntity releaseEntity = releaseDao.find(assetEntity.getReleaseId());
        NotFound.notNull(releaseEntity, input.getReleaseId());

        ProjectEntity projectEntity = projectDao.find(releaseEntity.getProjectId());
        NotFound.notNull(projectEntity, input.getReleaseId());

        namespacePolicy.assertPermission(projectEntity, Action.UPDATE);

        AssetEntity asset = assetDao.findByReleaseIdAndName(releaseEntity.getId(), assetEntity.getName());
        if (asset != null) {
            Assert.isTrue(AssetState.DRAFT.equals(asset.state), "存在同名文件");
            assetDao.delete(asset);
        }

        assetEntity.setCreatedById(viewerId());
        return assetDao.create(assetEntity);
    }

    public AssetEntity update(UpdateAssetInput input) {
        AssetEntity assetEntity = assetDao.find(input.id());
        NotFound.notNull(assetEntity, input.getId());

        ReleaseEntity releaseEntity = releaseDao.find(assetEntity.getReleaseId());
        NotFound.notNull(releaseEntity, input.getId());

        ProjectEntity projectEntity = projectDao.find(releaseEntity.getProjectId());
        NotFound.notNull(projectEntity, input.getId());

        namespacePolicy.assertPermission(projectEntity, Action.UPDATE);

        assetEntity.setName(input.getName());
        return assetDao.update(assetEntity);
    }

    public Resource find(Integer id) {
        // TODO 减少查询？
        AssetEntity assetEntity = assetDao.find(id);
        NotFound.notNull(assetEntity, id.toString());

        ReleaseEntity releaseEntity = releaseDao.find(assetEntity.getReleaseId());
        NotFound.notNull(releaseEntity, id.toString());

        ProjectEntity projectEntity = projectDao.find(releaseEntity.getProjectId());
        NotFound.notNull(projectEntity, id.toString());
        namespacePolicy.assertPermission(projectEntity, Action.READ);

        File file = properties.findAsset(assetEntity.getId()).toFile();
        NotFound.isTrue(file.exists(), id.toString());
        return new FileSystemResource(file);
    }

    public AssetEntity upload(Integer id, MultipartFile multipartFile) throws IOException {
        Assert.notNull(multipartFile, "上传文件出错");
        Assert.isTrue(multipartFile.getSize() <= 100 * MB, "文件大小超出 100MB");

        AssetEntity assetEntity = assetDao.find(id);
        NotFound.notNull(assetEntity, id.toString());
        Assert.isTrue(AssetState.DRAFT.equals(assetEntity.state), "state");

        ReleaseEntity releaseEntity = releaseDao.find(assetEntity.getReleaseId());
        NotFound.notNull(releaseEntity, id.toString());

        ProjectEntity projectEntity = projectDao.find(releaseEntity.getProjectId());
        NotFound.notNull(projectEntity, id.toString());
        namespacePolicy.assertPermission(projectEntity, Action.UPDATE);

        File outFile = properties.findAsset(assetEntity.getId()).toFile();
        Assert.isTrue(!outFile.exists(), "file exist");
        File parentFile = outFile.getParentFile();
        Assert.isTrue(parentFile.exists() || parentFile.mkdirs(), "创建目录失败");
        multipartFile.transferTo(outFile);

        assetEntity.setState(AssetState.PUBLISHED);
        assetEntity.setName(multipartFile.getOriginalFilename());
        assetEntity.setContentType(multipartFile.getContentType());
        assetEntity.setSize(multipartFile.getSize());
        assetEntity.setCreatedById(viewerId());
        // TODO
        // assetEntity.setSha256();
        return assetDao.update(assetEntity);
    }

    public AssetEntity delete(DeleteAssetInput input) {
        AssetEntity assetEntity = assetDao.find(input.id());
        NotFound.notNull(assetEntity, input.getId());

        ReleaseEntity releaseEntity = releaseDao.find(assetEntity.getReleaseId());
        NotFound.notNull(releaseEntity, input.getId());

        ProjectEntity projectEntity = projectDao.find(releaseEntity.getProjectId());
        NotFound.notNull(projectEntity, input.getId());

        namespacePolicy.assertPermission(projectEntity, Action.UPDATE);

        File file = properties.findAsset(assetEntity.getId()).toFile();
        if (file.exists()) file.delete();

        return assetDao.delete(assetEntity);
    }
}
