package dev.gitone.server.mappers;

import dev.gitone.server.controllers.assets.AssetFilter;
import dev.gitone.server.controllers.assets.AssetPage;
import dev.gitone.server.entities.AssetEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AssetMapper extends NodeMapper<Integer, AssetEntity> {

    AssetEntity findByReleaseIdAndName(@Param("releaseId") Integer releaseId, @Param("name") String name);

    List<AssetEntity> findAll(@Param("filter") AssetFilter filter, @Param("page") AssetPage page);
}
