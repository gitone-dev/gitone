package dev.gitone.server.controllers.assets;

import dev.gitone.server.Order;
import dev.gitone.server.OrderDirection;
import lombok.Data;

@Data
public class AssetOrder implements Order {

    private AssetOrderField field = AssetOrderField.CREATED_AT;

    private OrderDirection direction = OrderDirection.ASC;
}
