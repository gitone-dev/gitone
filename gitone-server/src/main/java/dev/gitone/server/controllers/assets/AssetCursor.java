package dev.gitone.server.controllers.assets;

import com.fasterxml.jackson.annotation.JsonInclude;
import dev.gitone.server.CustomCursor;
import dev.gitone.server.entities.AssetEntity;
import graphql.relay.ConnectionCursor;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssetCursor extends CustomCursor {

    public static ConnectionCursor create(AssetEntity node, AssetOrder order) {
        AssetCursor cursor = new AssetCursor();
        cursor.setId(node.getId());
        switch (order.getField()) {
            case CREATED_AT -> cursor.setCreatedAt(node.getCreatedAt());
            case NAME -> cursor.setName(node.getName());
        }
        return cursor;
    }

    private Integer id;

    private OffsetDateTime createdAt;

    private String name;
}
