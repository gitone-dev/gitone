package dev.gitone.server.controllers.assets.inputs;

import dev.gitone.server.controllers.Relay;
import dev.gitone.server.entities.AssetEntity;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class UpdateAssetInput {

    @NotEmpty
    private String id;
    @NotEmpty
    private String name;

    public Integer id() {
        return Relay.fromGlobalId(AssetEntity.TYPE, id).id();
    }
}
