package dev.gitone.server.controllers.releases.inputs;

import dev.gitone.server.entities.ReleaseEntity;
import dev.gitone.server.entities.ReleaseState;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class CreateReleaseInput {

    @NotBlank
    private String fullPath;
    @NotBlank
    private String tagName;
    @NotBlank
    private String title;

    private String description;

    private ReleaseState state = ReleaseState.PUBLISHED;

    private Boolean latest;

    public ReleaseEntity entity() {
        ReleaseEntity entity = new ReleaseEntity();
        entity.setTagName(tagName);
        entity.setTitle(title);
        entity.setDescription(description);
        entity.setState(state);
        if (Boolean.TRUE.equals(latest) && ReleaseState.PUBLISHED.equals(state)) {
            entity.setLatest(OffsetDateTime.now());
        }
        return entity;
    }
}
