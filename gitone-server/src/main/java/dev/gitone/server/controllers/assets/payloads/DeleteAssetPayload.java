package dev.gitone.server.controllers.assets.payloads;

import dev.gitone.server.entities.AssetEntity;

public record DeleteAssetPayload(AssetEntity asset) {
}
