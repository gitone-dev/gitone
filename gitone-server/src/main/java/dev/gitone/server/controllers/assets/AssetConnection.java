package dev.gitone.server.controllers.assets;

import dev.gitone.server.CustomConnection;
import dev.gitone.server.entities.AssetEntity;
import graphql.relay.ConnectionCursor;

import java.util.List;

public class AssetConnection extends CustomConnection<AssetEntity, AssetPage> {

    public AssetConnection(List<AssetEntity> data, AssetPage page) {
        super(data, page);
    }

    @Override
    protected ConnectionCursor createCursor(AssetEntity node) {
        return AssetCursor.create(node, page.getOrder());
    }
}
