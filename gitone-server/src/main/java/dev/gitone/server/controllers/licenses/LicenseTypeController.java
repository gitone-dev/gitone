package dev.gitone.server.controllers.licenses;

import dev.gitone.server.controllers.Relay;
import dev.gitone.server.models.git.License;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

@SchemaMapping(typeName = License.TYPE)
@Controller
public class LicenseTypeController {

    @SchemaMapping
    public String id(License license) {
        return Relay.toGlobalId(License.TYPE, license.getId());
    }
}
