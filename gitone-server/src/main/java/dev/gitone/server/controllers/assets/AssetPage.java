package dev.gitone.server.controllers.assets;

import com.fasterxml.jackson.core.type.TypeReference;
import dev.gitone.server.CustomCursor;
import dev.gitone.server.OrderPage;

public class AssetPage extends OrderPage<AssetCursor> {

    private final AssetOrder order;

    public AssetPage(Integer first, String after, AssetOrder order) {
        super(first, after);
        this.order = order;
    }

    @Override
    public AssetPage validate() {
        super.validate();
        return this;
    }

    @Override
    protected AssetCursor createCursor(String cursor) {
        return CustomCursor.create(cursor, new TypeReference<>() {});
    }

    @Override
    public AssetOrder getOrder() {
        return order;
    }
}
