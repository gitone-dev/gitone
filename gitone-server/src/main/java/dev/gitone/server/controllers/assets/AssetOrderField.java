package dev.gitone.server.controllers.assets;

public enum AssetOrderField {
    CREATED_AT,
    NAME,
}
