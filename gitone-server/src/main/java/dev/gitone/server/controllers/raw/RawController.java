package dev.gitone.server.controllers.raw;

import dev.gitone.server.config.exception.NotFound;
import dev.gitone.server.daos.ProjectDao;
import dev.gitone.server.entities.ProjectEntity;
import dev.gitone.server.models.git.GitBlob;
import dev.gitone.server.models.git.GitRepository;
import dev.gitone.server.models.git.RevisionPath;
import dev.gitone.server.policies.Action;
import dev.gitone.server.policies.NamespacePolicy;
import lombok.AllArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@AllArgsConstructor
@RestController
public class RawController {

    private final ProjectDao projectDao;

    private final NamespacePolicy namespacePolicy;

    @GetMapping("/{path0}/{path1}/-/raw/{*star}")
    public ResponseEntity<Resource> raw(@PathVariable String path0,
                                        @PathVariable String path1,
                                        @PathVariable String star) throws IOException {
        String fullPath = String.join("/", path0, path1);
        return raw(fullPath, star);
    }

    @GetMapping("/{path0}/{path1}/{path2}/-/raw/{*star}")
    public ResponseEntity<Resource> raw(@PathVariable String path0,
                                        @PathVariable String path1,
                                        @PathVariable String path2,
                                        @PathVariable String star) throws IOException {
        String fullPath = String.join("/", path0, path1, path2);
        return raw(fullPath, star);
    }

    @GetMapping("/{path0}/{path1}/{path2}/{path3}/-/raw/{*star}")
    public ResponseEntity<Resource> raw(@PathVariable String path0,
                                        @PathVariable String path1,
                                        @PathVariable String path2,
                                        @PathVariable String path3,
                                        @PathVariable String star) throws IOException {
        String fullPath = String.join("/", path0, path1, path2, path3);
        return raw(fullPath, star);
    }

    @GetMapping("/{path0}/{path1}/{path2}/{path3}/{path4}/-/raw/{*star}")
    public ResponseEntity<Resource> raw(@PathVariable String path0,
                                        @PathVariable String path1,
                                        @PathVariable String path2,
                                        @PathVariable String path3,
                                        @PathVariable String path4,
                                        @PathVariable String star) throws IOException {
        String fullPath = String.join("/", path0, path1, path2, path3, path4);
        return raw(fullPath, star);
    }

    private ResponseEntity<Resource> raw(String fullPath, String star) throws IOException {
        ProjectEntity projectEntity = projectDao.findByFullPath(fullPath);
        namespacePolicy.assertPermission(projectEntity, Action.READ);
        GitRepository gitRepository = new GitRepository(projectEntity);

        RevisionPath revisionPath = new RevisionPath(gitRepository, star.substring(1));
        revisionPath.extract();

        GitBlob blob = GitBlob.find(gitRepository, revisionPath.getRevision(), revisionPath.getPath());
        NotFound.notNull(blob, star);

        return ResponseEntity.ok()
                .headers(headers -> headers
                        .setContentDisposition(ContentDisposition
                                .inline()
                                .filename(blob.getName())
                                .build()))
                .contentType(blob.mediaType())
                .body(new InputStreamResource(blob.inputStream()));
    }
}
