package dev.gitone.server.controllers.releases;

import dev.gitone.server.entities.ReleaseState;
import lombok.Data;

import java.util.Collection;
import java.util.Set;

@Data
public class ReleaseFilter {

    @Data
    public static class By {

        private String query;

        public ReleaseFilter filter() {
            ReleaseFilter filter = new ReleaseFilter();
            filter.setQuery(query);
            filter.setStates(Set.of(ReleaseState.PUBLISHED, ReleaseState.PREVIEW));
            return filter;
        }
    }

    private Integer projectId;

    private String query;

    private Collection<ReleaseState> states;
}
