package dev.gitone.server.controllers.releases;

import dev.gitone.server.config.CustomProperties;
import dev.gitone.server.config.exception.NotFound;
import dev.gitone.server.daos.AssetDao;
import dev.gitone.server.daos.ProjectDao;
import dev.gitone.server.daos.ReleaseDao;
import dev.gitone.server.entities.AssetEntity;
import dev.gitone.server.entities.ProjectEntity;
import dev.gitone.server.entities.ReleaseEntity;
import dev.gitone.server.policies.Action;
import dev.gitone.server.policies.NamespacePolicy;
import lombok.AllArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

@AllArgsConstructor
@RestController
public class ReleaseController {

    private final ProjectDao projectDao;

    private final NamespacePolicy namespacePolicy;

    private final ReleaseDao releaseDao;

    private final AssetDao assetDao;

    private final CustomProperties properties;

    @GetMapping("/{path0}/{path1}/-/releases/download/{*star}")
    public ResponseEntity<Resource> download(@PathVariable String path0,
                                             @PathVariable String path1,
                                             @PathVariable String star) {
        String fullPath = String.join("/", path0, path1);
        return download(fullPath, star);
    }

    @GetMapping("/{path0}/{path1}/{path2}/-/releases/download/{*star}")
    public ResponseEntity<Resource> download(@PathVariable String path0,
                                             @PathVariable String path1,
                                             @PathVariable String path2,
                                             @PathVariable String star) {
        String fullPath = String.join("/", path0, path1, path2);
        return download(fullPath, star);
    }

    @GetMapping("/{path0}/{path1}/{path2}/{path3}/-/releases/download/{*star}")
    public ResponseEntity<Resource> download(@PathVariable String path0,
                                             @PathVariable String path1,
                                             @PathVariable String path2,
                                             @PathVariable String path3,
                                             @PathVariable String star) {
        String fullPath = String.join("/", path0, path1, path2, path3);
        return download(fullPath, star);
    }

    @GetMapping("/{path0}/{path1}/{path2}/{path3}/{path4}/-/releases/download/{*star}")
    public ResponseEntity<Resource> download(@PathVariable String path0,
                                             @PathVariable String path1,
                                             @PathVariable String path2,
                                             @PathVariable String path3,
                                             @PathVariable String path4,
                                             @PathVariable String star) {
        String fullPath = String.join("/", path0, path1, path2, path3, path4);
        return download(fullPath, star);
    }

    private ResponseEntity<Resource> download(String fullPath, String star) {
        ProjectEntity projectEntity = projectDao.findByFullPath(fullPath);
        namespacePolicy.assertPermission(projectEntity, Action.READ);

        int idx = star.lastIndexOf("/");
        Assert.isTrue(idx > 0 && idx < star.length() - 1, star);

        String tagName = star.substring(1, idx);
        ReleaseEntity releaseEntity = releaseDao.findByProjectIdAndTagName(projectEntity.getId(), tagName);
        NotFound.notNull(releaseEntity, star);

        String name = star.substring(idx + 1);
        AssetEntity assetEntity = assetDao.findByReleaseIdAndName(releaseEntity.getId(), name);
        NotFound.notNull(assetEntity, star);

        File file = properties.findAsset(assetEntity.getId()).toFile();
        NotFound.isTrue(file.exists(), star);

        return ResponseEntity.ok()
                .headers(headers -> headers
                        .setContentDisposition(ContentDisposition
                                .inline()
                                .filename(name)
                                .build()))
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(new FileSystemResource(file));
    }
}
