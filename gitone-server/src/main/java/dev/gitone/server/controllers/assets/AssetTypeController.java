package dev.gitone.server.controllers.assets;

import dev.gitone.server.controllers.Relay;
import dev.gitone.server.entities.AssetEntity;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

@AllArgsConstructor
@Controller
@SchemaMapping(typeName = AssetEntity.TYPE)
public class AssetTypeController {

    @SchemaMapping
    public String id(AssetEntity assetEntity) {
        return Relay.toGlobalId(AssetEntity.TYPE, assetEntity.getId());
    }
}
