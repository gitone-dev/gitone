package dev.gitone.server.controllers.assets;

import dev.gitone.server.entities.AssetEntity;
import dev.gitone.server.entities.Role;
import dev.gitone.server.services.AssetService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Duration;

@AllArgsConstructor
@RestController
public class AssetController {

    private final AssetService assetService;

    @GetMapping("/reset/assets/{id}")
    public ResponseEntity<Resource> asset(@PathVariable Integer id) throws IOException {
        Resource resource = assetService.find(id);
        return resource(resource);
    }

    @Secured({ Role.ROLE_USER })
    @PutMapping("/rest/assets/{id}")
    public AssetEntity asset(@PathVariable Integer id, @RequestParam("file") MultipartFile file) throws IOException {
        return assetService.upload(id, file);
    }

    private ResponseEntity<Resource> resource(Resource resource) throws IOException {
        return ResponseEntity.ok()
                .cacheControl(CacheControl.maxAge(Duration.ofMinutes(5)).cachePublic())
                .lastModified(resource.lastModified())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }
}
