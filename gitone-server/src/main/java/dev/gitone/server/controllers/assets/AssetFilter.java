package dev.gitone.server.controllers.assets;

import dev.gitone.server.entities.AssetState;
import lombok.Data;

import java.util.Collection;
import java.util.Set;

@Data
public class AssetFilter {

    @Data
    public static class By {

        private String query;

        public AssetFilter filter() {
            AssetFilter filter = new AssetFilter();
            filter.setQuery(query);
            filter.setStates(Set.of(AssetState.PUBLISHED));
            return filter;
        }
    }

    private Integer releaseId;

    private String query;

    private Collection<AssetState> states;
}
