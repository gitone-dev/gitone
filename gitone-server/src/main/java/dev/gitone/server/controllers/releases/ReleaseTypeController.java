package dev.gitone.server.controllers.releases;

import dev.gitone.server.controllers.Relay;
import dev.gitone.server.controllers.assets.AssetConnection;
import dev.gitone.server.controllers.assets.AssetFilter;
import dev.gitone.server.controllers.assets.AssetOrder;
import dev.gitone.server.controllers.assets.AssetPage;
import dev.gitone.server.daos.AssetDao;
import dev.gitone.server.daos.ProjectDao;
import dev.gitone.server.daos.ReleaseDao;
import dev.gitone.server.daos.UserDao;
import dev.gitone.server.entities.AssetEntity;
import dev.gitone.server.entities.ProjectEntity;
import dev.gitone.server.entities.ReleaseEntity;
import dev.gitone.server.entities.UserEntity;
import dev.gitone.server.models.git.GitRepository;
import dev.gitone.server.models.git.GitTag;
import lombok.AllArgsConstructor;
import org.checkerframework.checker.fenum.qual.AwtAlphaCompositingRule;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.BatchMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
@Controller
@SchemaMapping(typeName = ReleaseEntity.TYPE)
public class ReleaseTypeController {

    private final ProjectDao projectDao;

    private final UserDao userDao;

    private final ReleaseDao releaseDao;

    private final AssetDao assetDao;

    @SchemaMapping
    public String id(ReleaseEntity releaseEntity) {
        return Relay.toGlobalId(ReleaseEntity.TYPE, releaseEntity.getId());
    }

    @SchemaMapping
    public Boolean latest(ReleaseEntity releaseEntity) {
        // TODO 避免重复查询，能否复用查询？
        ReleaseEntity latest = releaseDao.findLatest(releaseEntity.getProjectId());
        return latest != null && latest.getId().equals(releaseEntity.getId());
    }

    @SchemaMapping
    public GitTag tag(ReleaseEntity releaseEntity) throws IOException {
        // TODO 避免重复查询
        ProjectEntity projectEntity = projectDao.find(releaseEntity.getProjectId());
        GitRepository repository = new GitRepository(projectEntity);
        return GitTag.find(repository, releaseEntity.getTagName());
    }

    @SchemaMapping
    public AssetConnection assets(
            ReleaseEntity releaseEntity,
            @Argument Integer first,
            @Argument String after,
            @Argument AssetFilter.By filterBy,
            @Argument AssetOrder orderBy) {
        AssetFilter filter = Objects.requireNonNullElse(filterBy, new AssetFilter.By()).filter();
        filter.setReleaseId(releaseEntity.getId());

        AssetPage page = new AssetPage(first, after, orderBy).validate();
        List<AssetEntity> assets = assetDao.findAll(filter, page);
        return new AssetConnection(assets, page);
    }

    @BatchMapping
    public Map<ReleaseEntity, UserEntity> creator(List<ReleaseEntity> releases) {
        List<Integer> userIds = releases.stream().map(ReleaseEntity::getCreatedById).toList();
        Map<Integer, UserEntity> userMap = userDao.findByIds(userIds)
                .stream()
                .collect(Collectors.toMap(UserEntity::getId, Function.identity()));

        Map<ReleaseEntity, UserEntity> map = new HashMap<>();
        for (ReleaseEntity release : releases) {
            map.put(release, userMap.get(release.getCreatedById()));
        }
        return map;
    }
}
