package dev.gitone.server.controllers.repositories;

import dev.gitone.server.config.ContextKey;
import dev.gitone.server.config.exception.NotFound;
import dev.gitone.server.daos.ProjectDao;
import dev.gitone.server.entities.ProjectEntity;
import dev.gitone.server.models.git.GitRepository;
import dev.gitone.server.policies.Action;
import dev.gitone.server.policies.NamespacePolicy;
import graphql.GraphQLContext;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@AllArgsConstructor
@Controller
public class RepositoryQueryController {

    private final ProjectDao projectDao;

    private final NamespacePolicy namespacePolicy;

    @QueryMapping
    GitRepository repository(GraphQLContext context,  @Argument String fullPath) throws IOException {
        ProjectEntity projectEntity = projectDao.findByFullPath(fullPath);
        NotFound.notNull(projectEntity, fullPath);
        namespacePolicy.assertPermission(projectEntity, Action.READ);

        context.put(ContextKey.FULL_PATH, fullPath);
        return new GitRepository(projectEntity);
    }
}
