package dev.gitone.server.controllers.blob;

import dev.gitone.node.highlight.BlobLine;
import dev.gitone.node.highlight.BlobRequest;
import dev.gitone.node.markup.MarkupRequest;
import dev.gitone.server.config.ContextKey;
import dev.gitone.server.controllers.Relay;
import dev.gitone.server.models.git.GitBlob;
import dev.gitone.server.services.node.HighlightService;
import dev.gitone.server.services.node.MarkupService;
import graphql.GraphQLContext;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;

@AllArgsConstructor
@Controller
@SchemaMapping(typeName = GitBlob.TYPE)
public class BlobTypeController {

    private HighlightService highlightService;

    private MarkupService markupService;

    @SchemaMapping
    public String id(GitBlob gitBlob) {
        return Relay.toGlobalId(GitBlob.TYPE, gitBlob.getId());
    }

    @SchemaMapping
    public String rich(GraphQLContext context, GitBlob gitBlob) throws IOException {
        if (!gitBlob.isMarkup()) return null;

        String fullPath = context.getOrDefault(ContextKey.FULL_PATH, "");
        String revision = context.getOrDefault(ContextKey.REVISION, "");
        MarkupRequest request = MarkupRequest.newBuilder()
                .setFullPath(fullPath)
                .setRevision(revision)
                .setPath(gitBlob.getPath())
                .setText(new String(gitBlob.getData()))
                .build();

        return markupService.markup(request).getHtml();
    }

    @SchemaMapping
    public BlobLineConnection lines(
            GitBlob gitBlob,
            @Argument Integer first,
            @Argument String after
    ) throws IOException {
        if (!gitBlob.isViewable()) return null;

        BlobLinePage page = new BlobLinePage(first, after);
        // FIXME: 2023/11/28 语言识别
        BlobRequest request = BlobRequest.newBuilder()
                .setName(gitBlob.getName())
                .setText(new String(gitBlob.getData()))
                .build();
        // FIXME 越界
        List<BlobLine> lines = highlightService.blob(request).getBlobLinesList();
        if (page.getAfter() != null) {
            Integer number = page.getAfter().getNumber();
            if (number != null) lines = lines.subList(number, lines.size());
        }
        if (page.getFirst() != null) {
            lines = lines.subList(0, page.getFirst() + 1);
        }
        return new BlobLineConnection(lines, page);
    }
}
