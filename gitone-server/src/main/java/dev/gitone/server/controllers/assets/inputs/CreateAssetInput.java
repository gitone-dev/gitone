package dev.gitone.server.controllers.assets.inputs;

import dev.gitone.server.controllers.Relay;
import dev.gitone.server.entities.AssetEntity;
import dev.gitone.server.entities.ReleaseEntity;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class CreateAssetInput {

    @NotEmpty
    private String releaseId;
    @NotEmpty
    private String name;

    public Integer releaseId() {
        return Relay.fromGlobalId(ReleaseEntity.TYPE, releaseId).id();
    }

    public AssetEntity entity() {
        AssetEntity assetEntity = new AssetEntity();
        assetEntity.setReleaseId(releaseId());
        assetEntity.setName(name);
        return assetEntity;
    }
}
