package dev.gitone.server.controllers.assets;

import dev.gitone.server.controllers.assets.inputs.CreateAssetInput;
import dev.gitone.server.controllers.assets.inputs.DeleteAssetInput;
import dev.gitone.server.controllers.assets.inputs.UpdateAssetInput;
import dev.gitone.server.controllers.assets.payloads.CreateAssetPayload;
import dev.gitone.server.controllers.assets.payloads.DeleteAssetPayload;
import dev.gitone.server.controllers.assets.payloads.UpdateAssetPayload;
import dev.gitone.server.entities.AssetEntity;
import dev.gitone.server.entities.Role;
import dev.gitone.server.services.AssetService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;

@AllArgsConstructor
@Controller
public class AssetMutationController {

    private final AssetService assetService;

    @MutationMapping
    @Secured({ Role.ROLE_USER })
    public CreateAssetPayload createAsset(@Valid @Argument CreateAssetInput input) {
        AssetEntity assetEntity = assetService.create(input);
        return new CreateAssetPayload(assetEntity);
    }

    @MutationMapping
    @Secured({ Role.ROLE_USER })
    public UpdateAssetPayload updateAsset(@Valid @Argument UpdateAssetInput input) {
        AssetEntity assetEntity = assetService.update(input);
        return new UpdateAssetPayload(assetEntity);
    }

    @MutationMapping
    @Secured({ Role.ROLE_USER })
    public DeleteAssetPayload deleteAsset(@Valid @Argument DeleteAssetInput input) {
        AssetEntity assetEntity = assetService.delete(input);
        return new DeleteAssetPayload(assetEntity);
    }
}
