package dev.gitone.server.controllers.assets.inputs;

import dev.gitone.server.controllers.Relay;
import dev.gitone.server.entities.AssetEntity;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class DeleteAssetInput {

    @NotEmpty
    private String id;

    public Integer id() {
        return Relay.fromGlobalId(AssetEntity.TYPE, id).id();
    }
}
