package dev.gitone.server.controllers.releases.inputs;

import dev.gitone.server.controllers.Relay;
import dev.gitone.server.entities.ReleaseEntity;
import dev.gitone.server.entities.ReleaseState;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class UpdateReleaseInput {

    @NotBlank
    private String id;
    @NotBlank
    private String tagName;
    @NotBlank
    private String title;

    private String description;

    private ReleaseState state = ReleaseState.PUBLISHED;

    private Boolean latest;

    public Integer id() {
        return Relay.fromGlobalId(ReleaseEntity.TYPE, id).id();
    }

    public void update(ReleaseEntity entity) {
        entity.setTagName(tagName);
        entity.setTitle(title);
        entity.setDescription(description);
        entity.setState(state);
        if (ReleaseState.PREVIEW.equals(state)) {
            entity.setLatest(null);
        } else if (ReleaseState.PUBLISHED.equals(state) && Boolean.TRUE.equals(latest)) {
            entity.setLatest(OffsetDateTime.now());
        }
    }
}
