package dev.gitone.server.controllers.blame;

import dev.gitone.server.controllers.Relay;
import dev.gitone.server.models.BlameLine;
import dev.gitone.server.models.git.GitBlame;
import dev.gitone.server.models.git.GitBlob;
import dev.gitone.server.models.git.GitCommit;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Controller
@SchemaMapping(typeName = GitBlame.TYPE)
public class BlameTypeController {

    @SchemaMapping
    public String id(GitBlame gitBlame) {
        return Relay.toGlobalId(GitBlob.TYPE, gitBlame.getId());
    }

    @SchemaMapping
    public List<BlameLine> lines(GitBlame gitBlame) throws IOException {
        List<BlameLine> lines = new ArrayList<>();

        String lastCommitSha = null;
        BlameLine line = null;
        for (int i = 0; i < gitBlame.size(); i++) {
            GitCommit commit = gitBlame.getCommit(i);
            if (commit.getSha().equals(lastCommitSha)) continue;

            lastCommitSha = commit.getSha();
            if (line != null) line.setRight(i);

            line = new BlameLine(i, gitBlame.size());
            line.setCommit(commit);
            line.setSourcePath(gitBlame.getSourcePath(i));
            lines.add(line);
        }

        return lines;
    }
}
