package dev.gitone.server.entities;

import dev.gitone.server.config.mybatis.IntegerValue;

public enum AssetState implements IntegerValue {

    DRAFT(-1), PUBLISHED(0);

    private final int value;

    AssetState(int value) {
        this.value = value;
    }

    @Override
    public int value() {
        return value;
    }
}
