package dev.gitone.server.entities;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class AssetEntity implements TimestampNode<Integer> {

    public static final String TYPE = "Asset";

    private Integer id;

    private OffsetDateTime createdAt;

    private OffsetDateTime updatedAt;

    private Integer releaseId;

    private String name;

    private String contentType;

    private Long size = 0L;

    public AssetState state = AssetState.DRAFT;

    private byte[] sha256;

    private Integer createdById;
}
