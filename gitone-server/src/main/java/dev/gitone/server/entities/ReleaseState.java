package dev.gitone.server.entities;

import dev.gitone.server.config.mybatis.IntegerValue;

public enum ReleaseState implements IntegerValue {

    DRAFT(-1), PUBLISHED(0), PREVIEW(1);

    private final int value;

    ReleaseState(int value) {
        this.value = value;
    }

    @Override
    public int value() {
        return value;
    }
}
