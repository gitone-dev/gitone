package dev.gitone.server.daos;

import dev.gitone.server.controllers.assets.AssetFilter;
import dev.gitone.server.controllers.assets.AssetPage;
import dev.gitone.server.entities.AssetEntity;
import dev.gitone.server.mappers.AssetMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AssetDao extends TimestampDao<Integer, AssetEntity, AssetMapper> {

    public AssetDao(AssetMapper mapper) {
        super(mapper);
    }

    public AssetEntity findByReleaseIdAndName(Integer releaseId, String name) {
        return mapper.findByReleaseIdAndName(releaseId, name);
    }

    public List<AssetEntity> findAll(AssetFilter filter, AssetPage page) {
        return mapper.findAll(filter, page);
    }
}
