package dev.gitone.server.config;

public class ContextKey {

    public static final String FULL_PATH = "full_path";

    public static final String REVISION = "revision";
}
